#
# dashboard.example.org
#
upstream upstream_cc_org {
  server unix:/home/admin/cc/shared/pids/unicorn-cc_app.sock fail_timeout=0;
}


server {
  server_name_in_redirect on;
  server_name dashboard.example.org;
  listen 80;
  rewrite ^ https://$server_name$request_uri? permanent;
}


server {
  server_name dashboard.example.org;
  listen 443 ssl;
  ssl on;

  ssl_certificate     /usr/local/etc/nginx/certs/dash.crt;
  ssl_certificate_key /usr/local/etc/nginx/certs/dash.key;

  ssl_prefer_server_ciphers on;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_ecdh_curve secp384r1;

  # bettercypto.org
  ssl_ciphers 'EDH+CAMELLIA:EDH+aRSA:EECDH+aRSA+AESGCM:EECDH+aRSA+SHA384:EECDH+aRSA+SHA256:EECDH:+CAMELLIA256:+AES256:+CAMELLIA128:+AES128:+SSLv3:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!DSS:!RC4:!SEED:!ECDSA:CAMELLIA256-SHA:AES256-SHA:CAMELLIA128-SHA:AES128-SHA';

  # https://wiki.mozilla.org/Security/Server_Side_TLS
  #ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:ECDHE-RSA-RC4-SHA:ECDHE-ECDSA-RC4-SHA:AES128:AES256:RC4-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!3DES:!MD5:!PSK
  
  # Alt cipher list to eventually be tested
  # ssl_ciphers "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS +RC4 RC4";

  # Diffie-Hellman parameter for DHE ciphersuites, recommended 2048 bits
  ssl_dhparam /usr/local/etc/nginx/certs/dhparam_4096.pem;

  # Enable this if your want HSTS (recommended, but be careful)
  add_header Strict-Transport-Security max-age=31104000;

  # OCSP Stapling ---
  # fetch OCSP records from URL in ssl_certificate and cache them
  ssl_stapling on;
  ssl_stapling_verify on;

  ## verify chain of trust of OCSP response using Root CA and Intermediate certs
  ssl_trusted_certificate /usr/local/etc/nginx/certs/dash.crt;
  
  ### Hetzner Online AG installimage
  resolver 213.133.99.99 213.133.100.100 213.133.98.98;

  client_max_body_size 100M;

  root /home/admin/cc/current/public;

  access_log /home/admin/cc/shared/log/cc_org-access.log main;
  error_log  /home/admin/cc/shared/log/cc_org-error.log debug;

  # Location for the Upstream (a/k/a Unicorn or Mongrel)
  location @cc_org {
    proxy_pass http://upstream_cc_org;
    include vhosts/proxy.common-conf;
  }

  # set Expire header on assets: see http://developer.yahoo.com/performance/rules.html#expires
#  location ~ ^/(images|assets|javascripts|stylesheets)/ {
#    try_files  $uri /last_assets/$uri @cc_org;
#    expires 10y;
#  }

  location ~ ^/(images|assets|javascripts|stylesheets)/ {
    try_files  $uri /last_assets/$uri @cc_org;
    gzip_static on; # to serve pre-gzipped version
    expires max;
    add_header Cache-Control public;
  }

  location / {
    if (-f $document_root/system/maintenance.html) { return 503; }
    try_files  $uri $uri/index.html $uri.html @cc_org;
  }

  # HTTP Error handling.
  #
  # 404 - Resource Not found.
  error_page 404 /404.html;

  # 500 - Internal Error
  # 502 - Bad Gateway
  # %04 - Gateway Timeout
  error_page 500 502 504 /500.html;

  # 503 - Service Unavailable
  error_page 503 @503;
  recursive_error_pages on;

  location @503 {
    error_page 405 = /system/maintenance.html;
    # Serve static assets if found.
    if (-f $request_filename) {
      break;
    }
    rewrite ^(.*)$ /system/maintenance.html break;
  }
}
