class Source < ActiveRecord::Base
  self.table_name = "sources"

  def as_json(*args)
    super.tap { |hash| hash["name"] = hash.delete "source" }
  end

end
