class AkMailingTag < ActiveRecord::Base
  self.table_name = "core_mailing_tags"
  belongs_to :tag, :class_name => :AkTag
end
