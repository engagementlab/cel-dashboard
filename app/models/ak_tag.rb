class AkTag < ActiveRecord::Base
  self.table_name = "core_tag"

  has_and_belongs_to_many :mailings, :class_name => :AkMailing, :join_table => 'core_mailing_tags', :foreign_key => :tag_id, :association_foreign_key => :mailing_id
  has_and_belongs_to_many :pages,    :class_name => :AkPage,    :join_table => 'core_page_tags',    :foreign_key => :tag_id, :association_foreign_key => :page_id
end
