class ReferrerUrl < ActiveRecord::Base

  def self.find_or_create!(url)

    begin

       # u = URI.split('http://www.FooBar.com/a/b/c?q=qwsa#wyz')
       #  => ["http", nil, "www.FooBar.com", nil, nil, "/a/b/c", nil, "q=qwsa", "foo"]
      scheme, user_info, host, port, registry, path, opaque, query, fragment = URI.split(url)

      host = 'localhost' if host.nil?
      host = self.strip_www(host.downcase)

      u = host + path
      r = self.cache_read(u)

      if r.nil?
        r = self.find_by_url(u)
        if r.nil?
          r = self.new
          r.url = u
          r.save!
          self.cache_write(u, r) if r
        end
      end

      return r

    rescue => e
      Rails.logger.error e.message.to_s + "\n" + e.backtrace.join("\n")
      return nil
    end
  end

  private

  def self.strip_www(host)
    if host.index('www.') == 0
      host[4..-1] 
    else
      host
    end
  end

  def self.cache_read(key)
    Rails.cache.read("#{self.to_s}:#{key}")
  end

  def self.cache_write(key, data)
    Rails.cache.write("#{self.to_s}:#{key}", data)
  end

end
