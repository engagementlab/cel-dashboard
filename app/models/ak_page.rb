class AkPage < ActiveRecord::Base
  self.table_name = "core_page"

  # Disables Active Record's STI which by default uses a column called 'type'
  # (core_page has a column called 'type'...)
  self.inheritance_column = '__noinheritance__'

  has_and_belongs_to_many :tags, :class_name => :AkTag, :join_table => 'core_page_tags',  :foreign_key => :page_id, :association_foreign_key => :tag_id
  has_many :fields, :class_name => AkPagefield, :foreign_key => :parent_id
  has_many :actions, :class_name => AkAction, :foreign_key => :page_id

  def as_json(*args)
    super.tap { |hash| hash["name"] = hash.delete "title"}
  end

  def AkPage.id_from_name(name)
    pid = self.cache_read(name)

    if pid.nil?
      p = AkPage.select(:id).where(name: name).first
      if p
        pid = p.id
        self.cache_write(name, pid)
      else
        pid = 0
      end
    end

    return pid
  end

  private

  def self.cache_read(key)
    Rails.cache.read("#{self.to_s}:#{key}")
  end

  def self.cache_write(key, data)
    Rails.cache.write("#{self.to_s}:#{key}", data)
  end

end
