class AkAction < ActiveRecord::Base
  self.table_name = "core_action"
  has_many :actionfields, :class_name => AkActionfield, :foreign_key => :parent_id
  belongs_to :page, :class_name => AkPage, :foreign_key => :page_id
 end
