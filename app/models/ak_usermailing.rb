class AkUsermailing < ActiveRecord::Base
  self.table_name = "core_usermailing"
  has_one :subject, :class_name => "AkMailingSubject"

  self.inheritance_column = '__noinheritance__'

end
