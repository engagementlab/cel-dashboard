class AkMailingSubject < ActiveRecord::Base
  self.table_name = "core_mailingsubject"
  belongs_to :mailing, :class_name => AkMailing, :foreign_key => :mailing_id

  def as_json(*args)
    super.tap { |hash| hash["name"] = hash.delete "text"}
  end
end

