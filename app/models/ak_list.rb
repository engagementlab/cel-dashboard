class AkList < ActiveRecord::Base
  self.table_name = "core_list"

  default_scope { where("hidden = false").order('is_default desc, name') }

  def self.default
    self.where(is_default: true).first
  end

end