class User < ActiveRecord::Base
  rolify

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :client

  validates_presence_of :email
  validates_uniqueness_of :email, :scope => :client_id

  def super_admin?
    return client.super_admin?
  end



  def admin?
    has_role?(:admin)
  end

  def tz_identifier
    ActiveSupport::TimeZone.zones_map[time_zone].tzinfo.identifier
  end
  alias_method :tz, :tz_identifier

  def tz_offset
    ActiveSupport::TimeZone.zones_map[time_zone].formatted_offset
  end

  def soft_delete
    # assuming you have deleted_at column added already
    update_attribute(:active, false)
  end

  def is_active?
    return self.active
  end

  def active_for_authentication?
    super && self.is_active?
  end

  def inactive_message
    "Sorry, this account has been deactivated."
  end

end
