class AkPagefield < ActiveRecord::Base
  self.table_name = "core_pagefield"

  belongs_to :page, :class_name => "AkPage", :foreign_key => :parent_id
end
