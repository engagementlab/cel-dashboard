class AkActionfield < ActiveRecord::Base
  self.table_name = "core_actionfield"
  belongs_to :action, :class_name => AkAction, :foreign_key => :parent_id
end
