class TypeaheadController < ApplicationController

  def index
    query = params[:q]
    render :json => nil and return if query.blank?

    terms = query.split(",").select{|x| x.strip if x.strip.present? }

    case params[:field]

      when "domain"
        if query.nil? || terms.size == 0
          @domains = EmailDomain.select("domain", "id").where("email_count > 10").order(email_count: :desc)
        else
          if terms.size > 1 || terms[0].to_i.to_s == terms[0]
            @domains = EmailDomain.select("domain", "id").where("email_count > 10 and id in (?)", terms).order(email_count: :desc)
          else
            @domains = EmailDomain.select("domain", "id").where("email_count > 10 and lower(domain) like ?", '%' + query + '%').order(email_count: :desc)
          end
        end
        render :json => @domains


      when "user_source"
        if query.nil?
          @sources = Source.select("source", "id").order(user_count: :desc)
        else
          if terms.size > 1 || terms[0].to_i.to_s == terms[0]
            @sources = Source.select("source", "id").where("id in (?)", terms).order(user_count: :desc)
          else
            @sources = Source.select("source", "id").where("lower(source) like ?", '%' + query + '%').order(user_count: :desc)
          end
        end
        render :json => @sources


      when "tag"
        if query.nil? || terms.size == 0
          @tags = AkTag.select("name", "id").order(times_used: :desc)
        else
          if terms.size > 1 || terms[0].to_i.to_s == terms[0]
            @tags = AkTag.select("name", "id").where("id in (?)", terms).order(times_used: :desc)
          else
            @tags = AkTag.select("name", "id").where("lower(name) like ?", '%' + query + '%').order(times_used: :desc)
          end
        end
        render :json => @tags


      when "mailing"
        if query.nil? || terms.size == 0
          @mailings = AkMailingSubject.select("text || \' (\' || mailing_id || \')\' as text", "mailing_id as id")
        else
          if terms.size > 1 || terms[0].to_i.to_s == terms[0]
            @mailings = AkMailingSubject.select("text || \' (\' || mailing_id || \')\' as text", "mailing_id as id").where("mailing_id in (?)", terms)
          else
            @mailings = AkMailingSubject.select("text || \' (\' || mailing_id || \')\' as text", "mailing_id as id").where("lower(text) like ?", '%' + query + '%')
          end
        end

        render :json => @mailings


      when "page"
        if query.nil? || terms.size == 0
          @pages = AkPage.select("title", "id").order(title: :desc)
        else
          if terms.size > 1 || terms[0].to_i.to_s == terms[0]
            @pages = AkPage.select("title", "id").where("id in (?)", terms).order(title: :desc)
          else
            @pages = AkPage.select("title", "id").where("lower(title) like ?", '%' + query + '%').order(title: :desc)
          end
        end
        render :json => @pages


      else
        render :json => nil
  
    end
  end

end
