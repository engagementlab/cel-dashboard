window.initTypeAhead = (field, q) ->
  selector = $("#" + field + "_ids")
  url = "/typeahead?field=" + field
  if q == null
    selector.tokenInput(url, theme: "facebook", preventDuplicates: true);
  else
    $.getJSON url, {q: q}, (data) ->
      selector.tokenInput(url, {preventDuplicates: true, theme: "facebook", prePopulate: data})

resetFilters = () ->
  $("#tableFilter input[type=search], #tableFilter input[type=date]").each ->
    $(@).val("")
  $("#tableFilter").submit();


isPageSignificant = (page) ->
  parseFloat(page['actions']) >= (Data.pageSignificanceThreshold * parseFloat(page['action_avg'])) ||
  $('#only-significant-pages input').prop('checked') == false


toggleUnsignificantPages = () ->
  $('tr.not-significant').toggle()
  initPageListSummryChart()


setClickHandlers = ->
 
  # Page List: graph time series page-id toggle
  $('.graph-ts.page-id').on 'click', ->
    pageListTimeSeriesChart()

  $('#only-significant-pages input').on 'click', ->
    toggleUnsignificantPages()

  $('button#reset').on 'click', (e) ->
    e.preventDefault()
    resetFilters.apply(@)


#
# Page List Summary Chart
#
initPageListSummryChart = () ->

  buildPageListSummaryChartData = (data, eventsWanted) ->
    chartData = []
    for row in data
      if isPageSignificant(row)
        for event in ["Actions", "Members", "New Subscribers", "Revenue"]

          if eventsWanted.indexOf(event) > -1
            key = event.replace(' ', '_').toLowerCase()
            count = parseInt(row[key])
            pct = parseInt(row[key]) / parseInt(row['list_size'])

            d = {}
            d['Page ID']    = parseInt(row['page_id'])
            d['Title']      = row['title']
            d['Event']      = event
            d['Percent']    = pct
            d['Count']      = count
            chartData.push(d)
    return chartData

  setupChart = () ->
    window.paChartData.length = 0
    window.paChartData = window.paChartData.concat(buildPageListSummaryChartData(Data.pageListSummary, ["Actions", "Members"]))
    if typeof(paSummaryChart) == "undefined"
      paSvg = dimple.newSvg("#SummaryEventChart", "100%", "100%")
      window.paSummaryChart = new dimple.chart(paSvg, window.paChartData)
      paSummaryChart.setMargins("10%", "40px", "10%", "4px")
    window.paSummaryChart.data = window.paChartData
    if paSummaryChart.axes.length == 0 && paChartData.length > 0
      paAxis  = paSummaryChart.addCategoryAxis("x", ["Page ID", "Event"])
      paAxis.hidden = true
      window.paAxis2 = paSummaryChart.addCategoryAxis("x", ["Page ID", "Event"])
      window.paMeasure = paSummaryChart.addMeasureAxis("y", "Count")
      paSeries  = paSummaryChart.addSeries("Event", dimple.plot.bar, [paAxis, paMeasure])
      paSeries2 = paSummaryChart.addSeries("Event", null, [paAxis2, paMeasure])
      paSummaryChart.addLegend('50%', 5, '40%', 20, "right", paSeries)
      paSummaryChart.setMargins("10%", "40px", "10%", "60px");
      paSummaryChart.assignColor("Actions", "#27AE60", "#27AE60", 1);
      paSummaryChart.assignColor("Members", "#2C2D50", "#2C2D50", 1);

  renderChart = (noDataChanged) ->
    window.paSummaryChart.draw(0, noDataChanged);
    window.paAxis2.titleShape.remove()
    window.paMeasure.titleShape.remove()

  setupChart()
  renderChart(false)

  window.onresize = () ->
    renderChart(true)


#
# Page List Time Series Chart
#
pageListTimeSeriesChart = () ->

  showPageSummaryChart = () ->
    $('#SummaryTimeSeriesChart').hide()
    $('#SummaryEventChart').show()

  showPageTimeSeriesChart = () ->
    $('#SummaryEventChart').hide()
    $('#SummaryTimeSeriesChart').show()

  createTsChart = () ->
    if typeof(paTsSummaryChart) == "undefined"
      svg = dimple.newSvg("#SummaryTimeSeriesChart", '100%', '100%');
      window.paTsSummaryChart = new dimple.chart(svg, window.paTsChartData);

    if paTsSummaryChart.axes.length == 0 && paTsChartData.length > 0
      dateAxis = paTsSummaryChart.addCategoryAxis("x", "period")

      actionsAxis = paTsSummaryChart.addMeasureAxis("y", "actions")
      mbrsAxis    = paTsSummaryChart.addMeasureAxis(actionsAxis, "members")

      pas = paTsSummaryChart.addSeries(["page_id"], dimple.plot.line, [dateAxis, actionsAxis])
      pms = paTsSummaryChart.addSeries(["page_id"], dimple.plot.line, [dateAxis, mbrsAxis])

      paTsSummaryChart.addLegend('50%', 5, '40%', 20, "right", pas)

  updatePageTsChartData = (data) ->
    window.paTsChartData = window.paTsChartData.concat(data)

  renderChart = (noDataChanged = false) ->
    if window.paTsChartData.length > 0
      paTsSummaryChart.data = paTsChartData
      paTsSummaryChart.draw(0, noDataChanged)

  # TODO: Set updating icon before the post(), and unset after the post()
  getTsData = (pageIds) ->
    d = {page_ids: pageIds.join(',') }
    d['activity_start_dt'] = $('#activity_start_dt').val() if $('#activity_start_dt').val().length > 0
    d['activity_end_dt']   = $('#activity_end_dt').val()   if $('#activity_end_dt').val().length > 0
    d['mailing_ids']       = $('#mailing_ids').val()       if $('#mailing_ids').val().length > 0
    d['tag_ids']           = $('#tag_ids').val()           if $('#tag_ids').val().length > 0

    $.post("/page/time_series", d).done (json) ->
      for key in Object.keys(json)
        window.paTsData[key] = json[key]
        updatePageTsChartData(json[key])
      createTsChart()
      renderChart()

  checkedPageIds = for cb in $('.graph-ts.page-id:checked')
    cb.value

  if checkedPageIds.length == 0
    showPageSummaryChart()
    return  # Note early exit from function if no pages are checked

  window.paTsChartData.length = 0
  paTsDataNeeded = []

  showPageTimeSeriesChart()

  for pid in checkedPageIds
    if paTsData[pid] == undefined
      paTsDataNeeded.push(pid)
    else
      updatePageTsChartData(paTsData[pid])

  createTsChart()

  if paTsDataNeeded.length > 0
    getTsData(paTsDataNeeded)

  renderChart()

  window.onresize = () ->
    renderChart(true)

#
# Page Detail Chart
#
initPageDetailChart = () ->

  buildPageListSummaryChartData = (data, eventsWanted) ->
    chartData = []
    for row in data
      for event in ["Actions", "Members", "New Subscribers"]

        if eventsWanted.indexOf(event) > -1
          key = event.replace(' ', '_').toLowerCase()
          count = parseInt(row[key])

          d = {}
          d['period']       = row['period']
          d['period_epoch'] = parseInt(row['period_epoch'])
          d['Event']        = event
          d['Count']        = count
          d['Revenue']      = parseFloat(row['revenue'])
          chartData.push(d)

    return chartData

  setupChart = () ->
    window.paChartData.length = 0
    window.paChartData = buildPageListSummaryChartData(Data.pageDetail, ["Actions", "Members"])

    if typeof(paSummaryChart) == "undefined"
      paSvg = dimple.newSvg("#PageDetailEventChart", "100%", "100%")
      window.paSummaryChart = new dimple.chart(paSvg, window.paChartData)
      paSummaryChart.setMargins("10%", "40px", "10%", "4px")

    window.paSummaryChart.data = window.paChartData

    if paSummaryChart.axes.length == 0 && paChartData.length > 0
      paAxis = paSummaryChart.addCategoryAxis("x", ["period", "Event"])
      window.paMeasure = paSummaryChart.addMeasureAxis("y", "Count")
      paSeries = paSummaryChart.addSeries("Event", dimple.plot.bar,  [paAxis, paMeasure])

      paSummaryChart.addLegend('50%', 5, '40%', 20, "right", paSeries)
      paSummaryChart.setMargins("10%", "40px", "10%", "60px");
      paSummaryChart.assignColor("Actions", "#27AE60", "#27AE60", 1);
      paSummaryChart.assignColor("Members", "#2C2D50", "#2C2D50", 1);

      if (akPageType == 'Donation')
        prAxis = paSummaryChart.addCategoryAxis("x", "period")
        prAxis.hidden = true
        window.paRevenue = paSummaryChart.addMeasureAxis("y", "Revenue")
        prSeries = paSummaryChart.addSeries("Event", dimple.plot.line, [prAxis, paRevenue])
        paSummaryChart.assignColor("Revenue", "#FFF10D", "#FFF10D", 1);


  renderChart = (noDataChanged) ->
    window.paSummaryChart.draw(0, noDataChanged);

  setupChart()
  renderChart(false)

  window.onresize = () ->
    renderChart(true)


#
# Page (Action|User) Source Chart
#
initPageSourceChart = () ->

  buildChartData = (data, eventsWanted) ->
    chartData = []
    for row in data
      for event in ["Actions", "New Members", "New Subscribers"]

        if eventsWanted.indexOf(event) > -1
          key = event.replace(' ', '_').toLowerCase()
          count = parseInt(row[key])

          d = {}
          d['Source']       = row['source']
          d['Event']        = event
          d['Count']        = count
          d['Revenue']      = parseFloat(row['revenue'])
          chartData.push(d)

    return chartData

  setupChart = () ->
    window.paChartData.length = 0
    window.paChartData = buildChartData(Data.pageSource, ["Actions", "New Members", "New Subscribers"])

    if typeof(paSummaryChart) == "undefined"
      paSvg = dimple.newSvg("#PageDetailEventChart", "100%", "100%")
      window.paSummaryChart = new dimple.chart(paSvg, window.paChartData)
      paSummaryChart.setMargins("10%", "40px", "10%", "4px")

    window.paSummaryChart.data = window.paChartData

    if paSummaryChart.axes.length == 0 && paChartData.length > 0
      paAxis = paSummaryChart.addCategoryAxis("x", ["Source", 'Event'])
      window.paMeasure = paSummaryChart.addMeasureAxis("y", "Count")
      paSeries = paSummaryChart.addSeries("Event", dimple.plot.bar,  [paAxis, paMeasure])

      paSummaryChart.addLegend('50%', 5, '40%', 20, "right", paSeries)
      paSummaryChart.setMargins("10%", "40px", "10%", "60px");
      # paSummaryChart.assignColor("Actions", "#8E44AD", "#8E44AD", 1);
      # paSummaryChart.assignColor("New Members", "#23E50", "#2C3E50", 1);
      # paSummaryChart.assignColor("New Subscribers", "#27AE60", "#27AE60", 1);

      paSummaryChart.assignColor("Actions", "#27AE60", "#27AE60", 1);
      paSummaryChart.assignColor("New Members", "#2C2D50", "#2C2D50", 1);
      paSummaryChart.assignColor("New Subscribers", "#E8B30C", "#E8B30C", 1);

      if (akPageType == 'Donation')
        prAxis = paSummaryChart.addCategoryAxis("x", "period")
        prAxis.hidden = true
        window.paRevenue = paSummaryChart.addMeasureAxis("y", "Revenue")
        prSeries = paSummaryChart.addSeries("Event", dimple.plot.line, [prAxis, paRevenue])

  renderChart = (noDataChanged) ->
    window.paSummaryChart.draw(0, noDataChanged);

  setupChart()
  renderChart(false)

  window.onresize = () ->
    renderChart(true)


$ ->
  window.paChartData = []
  window.paTsData = []
  window.paTsChartData = []
  window.paSummaryChart

  setClickHandlers()
  if Page == 'pageList'
    for f in ['tag', 'page', 'mailing']
      initTypeAhead(f, Data.filterParams[f])
    initPageListSummryChart()
  if Page == 'pageDetail'
    initPageDetailChart()
  if Page == 'pageActionSource' || Page == 'pageSserSource' 
    initPageSourceChart()

