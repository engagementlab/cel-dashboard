# adapted from Underscore library
# NaN is the only 'number' that doesn't equal itself
validDate = (obj) ->
  return new Date(obj).getTime() == new Date(obj).getTime()

tsParse = (ts) ->
  [d, t] = ts.split(' ')
  [y, mon, d] = d.split('-')
  [h, min, s] = t.split(':')
  dt = new Date(y, mon, d, h, min, s)
  return dt

toggleSubjects = (obj) ->
  mid = $(this).parent().data('mailing-id')
  $('tr.email-agg[data-mailing-id=' + mid + '] td.show-subjects').find('span').toggleClass('glyphicon-plus glyphicon-minus');
  $('tr.email-subject[data-mailing-id=' + mid + ']').toggleClass('show-subject')

clearFilters = () ->
  $("#tableFilter input[type=search], #tableFilter input[type=date]").each ->
    $(@).val("")
  $("#tableFilter").submit();

toggleAdvancedFields = () -> $(".slidingDiv").toggleClass('active')

#
# Mailing List Summary Chart
#
initMailingListSummryChart = () ->

  # after draw, use text() to select exact matches for 0 values
  # on measure charts.  need to make this less generalizable since
  # it really only applies to the condition in which we have
  # two charts stacked together.
  removeOriginPercent = () ->
    text = "0.00%"
    text2 = "0"
    zeroTick = $('.tick').filter () ->
       return $(@).text() == text || $(@).text() == text2
    zeroTick.remove()

  # TODO: Bug: If all metrics are unchecked, grouped bar effect breaks, 
  # and unsubs are charted directly under other metrics.
  # Not sure what the hack is here to fix this, since this is in itself a hack, 
  # but it might be a good time to revisit whether we want two separate charts.
  makeLegendInteractive = (chart, unsubChart, legend) ->
    # remove legend from chart, so that it doesn't update (i.e. remove metrics) when the chart updates
    chart.legends = []
    # grab all of our metric names to start
    filterValues = dimple.getUniqueValues(chart.data, "Event");

    legend.shapes.selectAll("rect").on "click", (e) -> 
      hide = false
      newFilters = [] #metrics that will be charted at the end
      metricClicked = e.aggField.slice(-1)[0]

      #TODO: Improve data structure for hidden data and associated metrics (for easy indexing)
      if typeof Data.hiddenMetricKeys == "undefined"
        Data.hiddenMetricKeys = [] 

      #loop through values that are visible, see if the one that clicked needs to be hidden
      filterValues.forEach (f) ->
        if (f == metricClicked) 
          hide = true #if hide, the item was already visible
          Data.hiddenMetricKeys.push(f)
          if typeof Data.hiddenOcaMetrics == "undefined" or typeof Data.hiddenUnsubMetrics == "undefined"
            #Grab the fake data and the real data from both charts, put them in separate caches.  
            Data.hiddenUnsubMetrics = dimple.filterData(unsubChart.data, "Event", metricClicked)
            Data.hiddenOcaMetrics = dimple.filterData(chart.data, "Event", metricClicked)
          else 
            Data.hiddenUnsubMetrics = Data.hiddenUnsubMetrics.concat(dimple.filterData(unsubChart.data, "Event", metricClicked))
            Data.hiddenOcaMetrics = Data.hiddenOcaMetrics.concat(dimple.filterData(chart.data, "Event", metricClicked))
        else
          newFilters.push(f)
      
      if hide 
        d3.select(this).style("opacity", 0.2)
      else
        d3.select(this).style("opacity", 0.8)
        if $.inArray(metricClicked, Data.hiddenMetricKeys) != -1
          Data.hiddenMetricKeys.splice($.inArray(metricClicked,Data.hiddenMetricKeys), 1)
        newFilters.push(metricClicked)
            
      filterValues = newFilters

      # if we are hiding a metric, filter it from the data and set that to the new chart data, then draw.
      # if we are unhiding a metric, remove the metric from the hidden cache and add to the chart data. then draw.
      if hide 
          unsubChart.data = dimple.filterData(unsubChart.data, "Event", filterValues)
          chart.data = dimple.filterData(chart.data, "Event", filterValues)
      else
        hiddenUnsubData = dimple.filterData(Data.hiddenUnsubMetrics, "Event", metricClicked)
        hiddenOcaData = dimple.filterData(Data.hiddenOcaMetrics, "Event", metricClicked)
        unsubChart.data = unsubChart.data.concat(hiddenUnsubData)
        chart.data = chart.data.concat(hiddenOcaData)
        Data.hiddenUnsubMetrics = dimple.filterData(Data.hiddenUnsubMetrics, "Event", Data.hiddenMetricKeys)
        Data.hiddenOcaMetrics = dimple.filterData(Data.hiddenOcaMetrics, "Event", Data.hiddenMetricKeys)
      renderChart(false)
      if $.inArray(metricClicked, ["Unsubs", "Unsub_Bounces", "Unsub_Complaints"]) >= 0
        showEveryNthGridline(unsubMeasure, 4)

  addAggregateData = () ->
    for d of Data.aggregate
      # Do not include data if there were no emails sent for the period
      if Data.aggregate[d]['sent'] > 0
        Data.aggregate[d]['mailing_id'] = d
        Data.aggregate[d]['started_at'] = "1999-12-31 11:59:59"
        Data.mailingListSummary.push(Data.aggregate[d])

  buildMailingListSummaryChartData = (data, eventsWanted) ->
    chartData = []
    for row in data
      for event in ["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Bounces", "Unsub_Complaints"]
        if eventsWanted.indexOf(event) > -1
          count = parseInt(row[event.toLowerCase()])
          pct = parseInt(row[event.toLowerCase()]) / parseInt(row['sent'])
          pct = -pct if $.inArray(event,  ['Unsubs', 'Unsub_Bounces', 'Unsub_Complaints']) >= 0
        else
          count = 0
          pct = 0

        d = {}
        d['Mailing ID'] = row['mailing_id']
        d['Event']      = event
        d['Percent']    = pct
        d['Count']      = count
        if event == 'Opens'
          d['Sent']     = parseInt(row['sent'])
        else
          d['Sent']     = 0

        d['Started At'] = tsParse(row['started_at'])
        chartData.push(d)
    return chartData

  # remove aggregate labels in the second x-axis and make remaining
  # mailing ids behave as anchor links.
  removeAggOcaAxisLabels = (data, axis) ->

    axis.shapes.selectAll('text')
        .attr('class', 'mid-axis')
        .style('cursor', 'pointer')
        .text (d, i) ->
      new_label = d
      if d < 0
        new_label = ""
      return new_label

    # Q: should this exist as an event in seteventhandlers?
    $('.mid-axis').on 'click', (e) ->
        mid = e.target.innerHTML
        window.location.href = '/email/' + mid + '/detail/'


  replaceUnsubAxisLabels = (data, axis) ->
    labels = []
    for d in data
      mid = parseInt(d['mailing_id'])
      # TODO: should have this linked to Data.aggregate somehow, weird that we have
      # to configure two separate things when changing aggregate data.
      switch mid
        when -1 then labels[d['mailing_id']] = "Week"
        when -2 then labels[d['mailing_id']] = "Month"
        when -3 then labels[d['mailing_id']] = "Quarter"
        when -4 then labels[d['mailing_id']] = "Year"
        else labels[d['mailing_id']] = d['started_at'] 

    axis.shapes.selectAll('text').text (d, i) ->

      if labels[d]
        new_label = labels[d]
        format = d3.time.format("%b %d")
        
        if validDate(new_label)
          new_label = format(new Date(labels[d])) 

      return new_label

  showEveryNthGridline = (axis, oneInEvery) ->
    if oneInEvery > 0 && axis.shapes.length > 0
      del = 0
      axis.shapes.selectAll("text").each (d) ->
        this.remove() if del % oneInEvery != 0; del += 1;
      del = 0
      axis.shapes.selectAll("line").each (d) ->
        this.remove() if del % oneInEvery != 0; del += 1;
      del = 0
      axis.gridlineShapes.selectAll("line").each (d) ->
        this.remove() if del % oneInEvery != 0; del += 1;

  buildTooltip = (e, svg, series) ->
    #config so you don't have to read my silly code!
    config = {
      borderWidth: 5 #pixels
      borderColor: e.selectedShape.attr("fill")
      bgColor: '#FFF'
      font: 'sans-serif'
      fontColor: '#222'
      fontSize: '11px'
      width: 150
      height: 55
    }

    if $.inArray(e.seriesValue[0], ["Unsubs", "Unsub_Complaints", "Unsub_Bounces"]) >= 0
      unsub = true

    #Get the properties of the selected shape
    cx = parseFloat(e.selectedShape.attr("x"))
    cy = parseFloat(e.selectedShape.attr("y"))
    height = parseFloat(e.selectedShape.attr("height")) #don't get this.

    #Set the size and position of the tooltip
    x = if cx + config.width + 10 < svg.attr("width") then cx + 10 else cx - config.width - 20
    y = if cy - config.height / 2 < 0 then 25 else 150

    #Create a group for the tooltip objects
    #TODO: get out of global namespace icky!
    tooltip = series.chart._tooltipGroup = svg.append("g").attr("class", "tooltip-wrapper")

    #Add a rectangle surrounding the text pop
    tooltip.append("rect")  
    .attr("x", x + 140)
    .attr("y", y - 5)
    .attr("width", config.width)
    .attr("height", config.height)
    .attr("rx", 5)
    .attr("ry", 5)
    .style("fill", config.bgColor)
    .style("stroke", config.borderColor)
    .style("stroke-width", config.borderWidth);

    switch parseInt(e.xValue)
      when -4 then mid = "Year"
      when -3 then mid = "Quarter"
      when -2 then mid = "Month"
      when -1 then mid = "Week"
      else mid = e.xValue
    
    if unsub
      # TODO: i believe there is a bug in dimple that is preventing the eventArgs object from passing the height of the 
      # value as it's yValue when the height in negative (i.e. for unsub, complaints, and bounces).  will follow up/.
      window.text = ['Metric: ' + e.seriesValue[0], 'Rate: ' + '-' + parseInt(e.selectedShape[0][0].attributes.height.nodeValue)/100.toFixed(2) + '%', 'Mailing ID: ' + mid]
    else
      window.text = ['Metric: ' + e.seriesValue[0], 'Rate: ' + (e.yValue * 100).toFixed(2) + '%', 'Mailing ID: ' + mid]
    
    tooltip.append('text')
           .selectAll('tspan').data(text)
           .enter()
           .append('tspan')
           .attr('x', x + 152)
           .attr('y', (d,i) -> return y + ((i + 1) * 13))
           .text((d) -> return d)
           .style("font-family", config.font)
           .style("font-size", config.fontSize)
           .style("color", config.fontColor)

    dropDest = series._dropLineOrigin()
    offset = if series._isStacked() then 1 else width / 2

    if not series.y._hasCategories() and dropDest.x != null 
      series.chart._tooltipGroup.append("line")
           .attr("x1", (if cx < dropDest.x then cx + width else cx))
           .attr("y1", (if unsub then height else cy))
           .attr("x2", (if cx < dropDest.x then cx + width else cx))
           .attr("y2", (if unsub then height else cy))
           .style("fill", "none").style("stroke", e.selectedShape.attr("fill"))
           .style("stroke-width", 2).style("stroke-dasharray", ("3, 3"))
           .style("opacity", e.selectedShape.attr("opacity"))
           .transition()
           .delay(750 / 2)
           .duration(750 / 2)
           .ease("linear")
           .attr("x2", (if x < dropDest.x then dropDest.x - 1 else dropDest.x + 1))
        #Added 1px offset to cater for svg issue where a transparent
        #group overlapping a line can sometimes hide it in some browsers
        #Issue #10
  
  chartDuration = 800 #Length (ms) for chart to fully render

  removeTooltip = (e, svg, series) ->
    #TODO: cache objects in DOM if they've been instantiated so we only get
    #one instance of a tooltip rather than rebuild?  
    if series.chart._tooltipGroup != null
      series.chart._tooltipGroup.remove()

  #here we filter the data to return non aggregate data, and then draw the chart right as soon as the initial chart is finished drawing    
  removeAggregateSends = (chart) ->
    ocaChart.series[1]._positionData = ocaChart.series[1]._positionData.filter((d) -> if d['x'] > 0 then return d) 
    ocaChart.draw(chartDuration + 1, true)

  renderChart = (init) ->
    ocaChart.draw(chartDuration);
    makeLegendInteractive(ocaChart,unsubChart,ocaLegend)
    removeAggregateSends(ocaChart)
    unsubChart.draw(chartDuration);
    ocaMeasure.titleShape.remove()
    ocaAxis2.titleShape.remove()
    unsubAxis.titleShape.remove()
    unsubMeasure.titleShape.remove()
    replaceUnsubAxisLabels(Data.mailingListSummary, unsubAxis)
    removeAggOcaAxisLabels(Data.mailingListSummary, ocaAxis2)
    removeOriginPercent()

  addAggregateData()

  ocaData = buildMailingListSummaryChartData(Data.mailingListSummary, ["Actions", "Clicks", "Opens"])
  ocaSvg = dimple.newSvg("#SummaryEventChart", "100%", "100%")
  window.ocaChart = new dimple.chart(ocaSvg, ocaData);

  ocaAxis  = ocaChart.addCategoryAxis("x", ["Mailing ID", "Event", "Sent"]);
  ocaAxis.addOrderRule('Started At')
  ocaAxis.addGroupOrderRule(["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Bounces", "Unsub_Complaints"])
  ocaAxis.hidden = true #we show this w/ unsub chart

  #We need this axis so that we don't plot the sent count per group element.
  window.ocaAxis2 = ocaChart.addCategoryAxis("x", ["Mailing ID"]);
  ocaAxis2.addOrderRule('Started At')
  
  ocaMeasure = ocaChart.addMeasureAxis("y", "Percent");
  ocaMeasure.tickFormat = ".2%"

  ocaMeasure2 = ocaChart.addMeasureAxis("y", "Sent");

  window.ocaSeries  = ocaChart.addSeries("Event", dimple.plot.bar, [ocaAxis, ocaMeasure]);
  ocaSeries.addEventHandler('mouseover', (e) -> buildTooltip(e, ocaSvg, ocaSeries))
  ocaSeries.addEventHandler('mouseleave', (e) -> removeTooltip(e, ocaSvg, ocaSeries))

  ocaSeries.addOrderRule(["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Bounces", "Unsub_Complaints"], true)

  #specific just to mailings list - plots send line
  window.ocaSeries2 = ocaChart.addSeries(null, dimple.plot.line, [ocaAxis2, ocaMeasure2])

  ocaLegend = ocaChart.addLegend('30%', 5, '70%', 20, "right", ocaSeries)

  ocaChart.assignColor("Opens", "#8E44AD", "#8E44AD", 1);
  ocaChart.assignColor("Clicks", "#2C3E50", "#2C3E50", 1);
  ocaChart.assignColor("Actions", "#27AE60", "#27AE60", 1);
  ocaChart.assignColor("Unsubs", "#C0392B", "#C0392B", 1);
  ocaChart.assignColor("Unsub_Complaints", "#FFA500", "#FFA500", 1);
  ocaChart.assignColor("Unsub_Bounces", "#CCCC00", "#CCCC00", 1);
  ocaChart.setMargins("10%", "60px", "10%", "0");

  unsubData = buildMailingListSummaryChartData(Data.mailingListSummary, ["Unsubs", "Unsub_Bounces", "Unsub_Complaints"])
  unsubSvg = dimple.newSvg("#SummaryUnsubChart", "100%", "100%")
  unsubChart = new dimple.chart(unsubSvg, unsubData);

  unsubAxis = unsubChart.addCategoryAxis("x", ["Mailing ID", "Event", "Sent"]);
  unsubAxis.addOrderRule('Started At')
  unsubAxis.addGroupOrderRule(["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Bounces", "Unsub_Complaints"])

  unsubMeasure = unsubChart.addMeasureAxis("y", "Percent");
  unsubMeasure.tickFormat = ".2%"

  window.unsubSeries = unsubChart.addSeries("Event", dimple.plot.bar, [unsubAxis, unsubMeasure]);
  unsubSeries.addOrderRule(["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Bounces", "Unsub_Complaints"], true)
  unsubSeries.addEventHandler('mouseover', (e) -> buildTooltip(e, unsubSvg, unsubSeries))
  unsubSeries.addEventHandler('mouseleave', (e) -> removeTooltip(e, unsubSvg, unsubSeries))


  unsubSeries2 = unsubChart.addSeries("Started_at", null, [unsubAxis, unsubMeasure]);

  unsubChart.assignColor("Unsubs", "#C0392B", "#C0392B", 1);
  unsubChart.assignColor("Unsub_Complaints", "#FFA500", "#FFA500", 1);
  unsubChart.assignColor("Unsub_Bounces", "#CCCC00", "#CCCC00", 1);

  
  unsubChart.setMargins("10%", "0", "10%", "60px");
  renderChart(true)
  showEveryNthGridline(unsubMeasure, 4)

  window.onresize = () ->
    renderChart(false)

#
# Mailing List Time Series Chart
#
mailingListTimeSeriesChart = () ->

  showMailingSummaryChart = () ->
    $('#SummaryTimeSeriesChart').hide()
    $('#SummaryEventChart').show()
    $('#SummaryUnsubChart').show()

  showMailingListTimeSeriesChart = () ->
    $('#SummaryEventChart').hide()
    $('#SummaryUnsubChart').hide()
    $('#SummaryTimeSeriesChart').show()

  createEventLegend = (chart) ->
    # access svg from dimple chart and roll our own legend, since Dimple doesn't really
    # support what we are trying to do with dashed lines.

    filterValues = dimple.getUniqueValues(chart.data, "event")
    window.legend = chart.svg.selectAll('.legend')
                          .data(filterValues)
                        .enter().append('g')
                          .attr('class', 'legend')
                          .attr('transform', (d, i) ->
                            return "translate(" + ((i * 55) + 80) + ",10)"
                          )

    legend.append('line')
          .attr('x1', 10)
          .attr('x2', 23)
          .attr('y1', 0)
          .attr('y2', 0)
          .style('stroke-width', '3')
          .style('stroke-dasharray', (d) -> 
            if d == "opens"
              return 0
            else if d == "clicks"
              return "6,2"
            else if d == "actions"
              return "2,3"
            else if d == "unsubs"
              return "4,4"
            else if d == "complaints"
              return "7,5" # TODO: needs some better styling
            else if d == "bounces"
              return "9,6" # TODO: needs some better styling
          )
          .style('stroke', '#BBD')
          .style('shape-rendering', 'crispedges')

    legend.append('text')
      .attr('x', 42)
      .attr('y', 0)
      .attr('dy', '.35em')
      .attr('height', 10)
      .style('font-size', '10px')
      .style('font-family', 'sans-serif')
      .style('text-anchor', 'middle')
      .style('cursor', 'pointer')
      .style('shape-rendering', 'crispedges')
      .style("opacity", (d) ->
          filterValues = ["opens"]
          return 0.3 if d != "opens"
        )
      .text((d) -> return d)

    legend.selectAll("line,text")
                 .on "click", (e) -> 
                  hide = false
                  newFilters = [] # metrics that will be charted at the end
                  metricClicked = e
                  svg = d3.select(@)

                  # loop through values that are visible, see if the one that clicked needs to be hidden
                  filterValues.forEach (f) ->
                    if (f == metricClicked) 
                      hide = true # if hide, the item was already visible
                    else
                      newFilters.push(f)

                  if hide 
                    svg.style("opacity", 0.3) 
                  else 
                    newFilters.push(metricClicked)
                    svg.style("opacity", 0.8)

                  filterValues = newFilters
                  chart.data = dimple.filterData(mailingListTsChartData, "event", filterValues)
                  chart.draw(200)
                  makeListLegendInteractive(mailingListTsDimpleChart, mailingListTsDimpleMailingLegend)

  makeListLegendInteractive = (chart, legend) ->
    # And then we want to extend the domain legend to trigger checkboxes,
    # creating a feeling of interactivity with the SVG.
    # has to be applied any time we redraw the chart, since we don't 
    # want to orphan the legend from Dimple's dynamic functionality.
    legend.shapes.selectAll('text, rect')
                 .style("cursor", "pointer")
                 .on "click", (e) ->
                  mid = e.aggField.slice(1,2)[0]
                  $('.mailing-id[value=' + mid + ']').trigger('click')

  createTsDimpleChart = () ->
    if typeof(mailingListTsDimpleChart) == "undefined"
      svg = dimple.newSvg("#SummaryTimeSeriesChart", '100%', '100%');
      window.mailingListTsDimpleChart = new dimple.chart(svg, window.mailingListTsChartData);
    if mailingListTsDimpleChart.axes.length == 0 && mailingListTsChartData.length > 0
      mailingListTsDimpleChart.setMargins("10%", "60px", "10%", "60px");
      dateAxis = mailingListTsDimpleChart.addTimeAxis("x", "period")
      dateAxis.tickFormat = "%I:%M%p"
      dateAxis.timePeriod = d3.time.hours
      dateAxis.timeInterval = 4
      eventAxis   = mailingListTsDimpleChart.addMeasureAxis("y", "count")
      mailingSeries = mailingListTsDimpleChart.addSeries(["event", "mailing_id"], dimple.plot.line, [dateAxis, eventAxis])
      #this is a `wat` variable name if I've ever seen one...TODO: simplify some of these names when DRY-ing up the code.
      window.mailingListTsDimpleMailingLegend = mailingListTsDimpleChart.addLegend('30%', 5, '70%', 20, "right", mailingSeries)

  updateMailingListTsChartData = (data) ->
    window.mailingListTsChartData = window.mailingListTsChartData.concat(data)

  renderChart = (init) ->
    if init
      #And one hack is replaced with another.  Because legends are based on data existing,
      #it isn't a trivial task to create an interactive legend that is essentially the 
      #opposite of this - http://dimplejs.org/advanced_examples_viewer.html?id=advanced_interactive_legends

      #We create a dummy dataset that is as lightweight as possible, draw it, 
      #orphan the legend from the chart and add interactivity to it, and then redraw with the 
      #appropriate data.  If everything goes according to plan, this should be good.

      dummyDataForLegend = [{
        event:"opens"
        period:0
      },
      {
        event:"clicks"
        period:0
      },
      {
        event:"actions"
        period:0
      },
      {
        event:"unsubs"
        period:0
      },
      {
        event:"complaints"
        period:0
      },
      {
        event:"bounces"
        period:0
      }]

      mailingListTsDimpleChart.data = dummyDataForLegend
      mailingListTsDimpleChart.draw()
      createEventLegend(mailingListTsDimpleChart)
      mailingListTsDimpleChart.data = dimple.filterData(mailingListTsChartData, "event", "opens")
      mailingListTsDimpleChart.draw()
      makeListLegendInteractive(mailingListTsDimpleChart, mailingListTsDimpleMailingLegend)
    else if window.mailingListTsChartData.length > 0
      mailingListTsDimpleChart.data = mailingListTsChartData
      mailingListTsDimpleChart.draw()
      makeListLegendInteractive(mailingListTsDimpleChart, mailingListTsDimpleMailingLegend)

  # TODO: Set updating icon before the post(), and unset after the post()
  getTsData = (mailingIds) ->
    $.post("/email/time_series", {mailing_ids: mailingIds.join(',') }).done (data) ->
      for key in Object.keys(data)
        window.mailingListTsData[key] = data[key]
        updateMailingListTsChartData(data[key])
      createTsDimpleChart()
      renderChart(true)


  checkedMailingIds = for cb in $('.graph-ts.mailing-id:checked')
    cb.value

  if checkedMailingIds.length == 0
    showMailingSummaryChart()
    return

  window.mailingListTsChartData.length = 0
  mailingListTsChartDataNeeded = []

  showMailingListTimeSeriesChart()

  for mid in checkedMailingIds
    if mailingListTsData[mid] == undefined
      mailingListTsChartDataNeeded.push(mid)
    else
      updateMailingListTsChartData(mailingListTsData[mid])

  createTsDimpleChart()

  if mailingListTsChartDataNeeded.length > 0
    getTsData(mailingListTsChartDataNeeded)
  else
    renderChart(true)

  window.onresize = () ->
    renderChart(false)

setClickHandlers = ->
  # Mailing List: subjects hide/show
  $('tr.email-agg td.show-subjects').on 'click', ->
    toggleSubjects.apply(@)
 
  # Mailing List: graph time series mailing-id toggle
  $('.graph-ts.mailing-id').on 'click', ->
    mailingListTimeSeriesChart()

  # Show 'advanced options' in filter box
  $('.show_hide').click () ->
    toggleAdvancedFields()

  $('button#reset').on 'click', (e) ->
    e.preventDefault()
    clearFilters.apply(@)

initTypeAhead = (field, q) ->
  selector = $("#" + field + "_ids")
  url = "/typeahead?field=" + field

  if (field == "domain" || field == "user_source") && q != null && !$('.slidingDiv').hasClass('active')
    toggleAdvancedFields()

  if q == null
    selector.tokenInput(url, theme: "facebook", preventDuplicates: true);
  else
    $.getJSON url, {q: q}, (data) ->
      selector.tokenInput(url, {preventDuplicates: true, theme: "facebook", prePopulate: data})

$ ->
  window.mailingListTsData = {}
  window.mailingListTsChartData = []
  window.mailingListTsDimpleChart

  setClickHandlers()
  for f in ['mailing', 'tag', 'domain', 'user_source']
    initTypeAhead(f, Data.filterParams[f])

  if window.Page == 'mailingList'
    initMailingListSummryChart()
