// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//

//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require moment
//= require navigation
//= require jquery.tokeninput
//= require chosen.jquery
