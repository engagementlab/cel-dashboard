module HomeHelper
  def is_active?(page_name)
    "active" if params[:action] == page_name
  end

  def email_class(rows, r, i)
    if r["subject_id"] == '0'
      if (i + 2) < rows.size && rows[i+2]['mailing_id'] == r["mailing_id"]
        "email-agg"
      else
        "email-single"
      end
    else
      "email-subject"
    end
  end
end
