# Welcome to the CEL Dashboard

[CEL](http://engagementlab.org) Dashboard is designed to support organizations that use the [ActionKit CRM](http://actionkit.com/). Dashboard simplifies many common ActionKit reporting activities and greatly simplifies some analytics such as trend analysis, email performance and comparison, page performance, and more.

Dashboard operates by sync'ing your ActionKit MySQL database to an PostgreSQL database, adding multiple indexes and new tables that facilitates reporting. The Database sync'er was designed around the structure and limitations of ActionKit CRM database. In practice, once Dashboard is sync'ing regularly, most ActionKit instances will require 5 minutes or less to sync. 

The overall process to setting up a Dashboard instance is:

1.  Install the required software and clone the Dashboard repository
2.  Update some config files
3.  Create a client
4.  Export the ActionKit database for your organization and then import it into the PostgreSQL database
5.  Add indexes
6.  Prep to perform an initial sync between ActionKit on your local instance
7.  Perform initial sync
8.  Config the sync'er to run continuously
9.  Config Nginx and related


### Historial Note

[Citizen Engagement Lab](http://engagementlab.org) supports multiple organizations such as [Color of Change](http://colorofchange.org/), [Ultraviolt](http://weareultraviolet.org/), [Demand Progress](https://demandprogress.org/) and more. As such, Dashboard was designed and built as a multi-tenant application -- one instance which supports multiple organizations. This should have little to no impact on deploying Dashboard for your organization. However, there may be a number of installation steps and/or administration tasks that do not apply or are not required for single organization deployment.

# Requirements

Dashboard requires fast disk I/O and significant memory allocated to the PostgreSQL database for reasonable performance. The exact amount of memory is dependent upon the size of your ActionKit database, though 32 GB of ram is a reasonable minimum. Additionally, SSD storage for the database files is strongly recommended.

Lastly, because of the database and other demands it is strongly recommended that Dashboard be deployed using a dedicated (managed or unmanaged) server and not via a cloud hosting provider such as Heroku. CEL has had good success using a dedicated [server](https://www.hetzner.de/hosting/produkte_rootserver/ex40ssd) from [Hetzner.de](https://www.hetzner.de/) which is also much more cost effective than Heroku.


# Dependencies

* PostgreSQL (version 9.3 or higher)
* Nginx (or other web server such as Apache)
  * Openssl
* Python V3
* Redis
* MySQL client (only required for building Ruby MySQL gem)
* Ruby 2.x
  * [rbenv](https://github.com/sstephenson/rbenv)


# Getting Started

## Notes

1. Dashboard was largely developed and deployed using Linux. If you develop using a Mac or Windows, please recognize that you will need to tailor the following instructions for your environment.
2. ActionKit database credentials - Dashboard connects to the read-only slave of your ActionKit MySQL database. As such it requires your organization's ActionKit database credentials: hostname, user ID and password. This information is available from ActionKit.
3. Database schema - Dashboard uses SQL schemas (name spaces) to partition data. This is done for a) each organization, and b) to maintain separation between the ActionKit tables and the Dashboard tables specific to an organization. At CEL, we use the suffix '_ak' for the ActionKit tables and '_cc' for the organization specific Dashboard tables. Example: orgname_ak and orgname_cc. Partitioning data by schema name greatly facilitates database administration.

## Dependencies

1. Install all of the dependencies
2. Update `postgresql.conf`. CEL's Dashboard instance (which has 64GB of ram) includes the following:
```
shared_buffers = 32GB
temp_buffers = 1GB
work_mem = 256MB
maintenance_work_mem = 1GB
checkpoint_segments = 64
seq_page_cost = 1.0
random_page_cost = 1.2
effective_cache_size = 16GB
default_statistics_target = 1000
from_collapse_limit = 10
join_collapse_limit = 10
log_timezone = 'UTC'

```

## Installation

### 1.0  Clone the repository
```
git clone git@bitbucket.org:engagementlab/cel-dashboard.git
```

### 2.0  Update gems
```
cd cel-dashboard
bundle
```

### 3.0  Update database config

```
cd config
cp database.yml-sample database.yml
cp ak_database.yml-sample ak_database.yml
```

#### 3.1  database.yml

Update database.yml as usual for a Rails application and your environment.


#### 3.2  ak_database.yml

Update ak_database.yml with ActionKit database hostname, user ID, and password for your organization. (If you want to support multiple ActionKit instances, create a block for each instance ak_database.yml.)

**Note:** Each ActionKit instance (even if there is only one) must be have it's own block. It is **strongly recommended** that you append '_ak' to the organization name for each ActionKit instance block.

```
schema_name_ak:
  adapter: mysql2
  encoding: utf8
  reconnect: true
  pool: 5
  timeout: 5000
  host: client-db.actionkit.com
  database: database_name
  username: user_id
  password: password
```


### 4.0  Create the database
```
rake db:create
rake db:migrate
rake db:seed
```

### 5.0  Create client for your organization
```
bundle exec ruby ./bin/create_client.rb "<organization name>" <schema_name> <admin_email_address>
```
**Note** <schema_name> + '_ak' must match the value you entered in `config/ak_database.yml`.


Example (presuming your organization name is `Petitions.org` and using `petitions` for the base schema name):
```
bundle exec ruby ./bin/create_client.rb "Petitions.org" petitions dashboard-admin@petitions.org
```

The above command will create a client with the name `Petitions.org`. It will also create two schemas: `petitions_ak` and `petitions_cc`. Lastly, it creates an admin user with the specified email address.

The temporary password for the created user is `ChangeMe321.`. Please update it via the admin interface to a more secure password.


### 6.0  Generate ActionKit export script
```
mkdir data
cd data
bundle exec ruby ../bin/gen_ak_mysqldump_cmds.rb schema_name_ak
```

**Note** the `schema_name_ak` in the above command must match the name you entered in `ak_database.yml'.


### 7.0  Execute the generated ActionKit export script
```
./<schemaname>_ak_export.sh
```

**Note** you will be prompted (twice) to enter the password for your ActionKit database. This is the password you entered into `config/ak_database.yml'.

Depending on the size of your ActionKit database, how busy the ActionKit servers are, and your network speed, this command may take a few minutes to an hour or longer.

Once the data is exported, a Python3 script will automatically run to convert the MySQL dump file into a format that can be imported into PostgreSQL,


### 8.0  Import ActionKit data
```
psql -U <username> -W <dbname> < <schemaname>_ak_import.pgsql
cd ..
```

Replace `<username>`, `<dbname>` and `<schemaname>` with the appropriate values.

Again, depending on the size of your ActionKit instance and the the speed of your local database, this command may take minutes to an hour or longer.


### 9.0  Add indexes to ActionKit tables
```
bundle exec ruby bin/ak_create_indexes.rb <schemaname>_ak
```

Again, depending on the size of your ActionKit instance and the the speed of your local database, this command may take minutes to an hour or longer.


### 10.0  Create Dashboard client specific tables
```
bundle exec rake apartment:migrate
```

This command created the Dashboard tables specific to your ActionKit instance.

([Apartment](https://github.com/mjsteckel/apartment) is a Ruby gem that enables multi-tenant schemas.)


### 11.0  Prepare for ActionKit sync'ing
```
bundle exec ruby ./bin/ak_sync.rb <schemaname>_ak prepare
```

This command will produce output, mostly a number of warning messages which can safely be ignored.

**Note** - the ak_sync.rb script will print a brief help message if it is executed without any options.


### 12.0  AK pre-sync check
```
bundle exec ruby ./bin/ak_sync.rb <schemaname>_ak check
```

This command should output something along the lines of:
```
check_schema(<schemaname>_ak, true)
check_schema(<schemaname>_ak): ok_to_sync= true
```


### 13.0   Perform initial sync of ActionKit datbase
```
bundle exec ruby ./bin/ak_sync.rb <schemaname>_ak sync --refresh
```

Depending on the size of your ActionKit instance and the speed of your local database, the execution of this command will vary from a few minutes to a few hours. The first sync takes much longer then subsequent syncs.

ak_sync.rb will output some status as it processes each table. Add `--verbose=6` to the above command if you would like to see detailed information about what ak_sync.rb is doing. Using verbose levels of 7+ will show you row level detail (not recommended!).


## Installation final steps

1.  Dashboard uses Unicorn as the application server. You will need to configure Nginx (or your preferred web server) to handle basic web requests. Example Nginx config files are in `etc/nginx/`.

2.  `bin/ak_sync.rb` must run periodically to refresh the local data from ActionKit. The sync'er usually takes less than 5 minutes to run. Creating a cron job to run the sync'er every 15 minutes or so should be sufficient. When ak_sync.rb starts it checks to see if another copy is already running, and if so, exits immediately.


# ak_sync.rb notes and trouble shooting

ak_sync.rb is fairly reliable but is not perfect. 

ActionKit periodically modifies the structure of their database (usually with major releases). Sometimes these changes are too much for ak_sync.rb to cope with and it fails (examples of causes for failures are data integrity differences between MySQL and PostgresSQL, differing support of UTF-8 character encoding between MYSQL/PostgresSQL).

Every time ak_sync.rb runs it inserts a row into sync_log. This is the first place to look if there is a problem. The following query displays the status of the last ak_sync.rb run for each ActionKit instance:
```
select sl.* from sync_log sl, (select max(x.id) id from sync_log x group by schema_name) AS latest where sl.id = latest.id order by sl.updated_at desc;
```

Sometimes after ActionKit has modified their database structure beyond what ak_sync.rb can cope with you will need to execute the following command to update Dashboard's meta data.
```
bundle exec ruby ./bin/ak_sync.rb <schemaname>_ak update-col-counts
```

Try re-sync'ing after the above command completes.


Occasionally, a unique key violation will cause ak_sync.rb to fail. The problem table should be indicated in `sync_log`. As long as the problem table **IS NOT** core_open, core_click, core_action, or core_usermailing, you can try the following:
```
bundle exec rails db
TRUNCATE <schamename>_ak.tablename RESTART IDENTITY;
exit
bundle exec ruby ./bin/ak_sync.rb <schemaname>_ak sync --refresh
```

Depending on the table truncated, the sync may complete in the normal time or it may take longer.

DO NOT truncate core_open, core_click, core_action, or core_usermailing. If any of these tables are causing the sync'er to fail you will need to re-export your ActionKint instance. See below.


If all else fails, you can drop the schema and re-export the ActionKit data. For instance:
```
bundle exec rails db
drop schema <schemaname>_ak cascade;
drop schema <schemaname>_cc cascade;
create schema <schemaname>_ak;
create schema <schemaname>_cc;
exit
```

Then complete the installation instructions starting at step 6 above.


# License

CEL Dashboard for ActionKit is released under the [GPL V3 License](http://opensource.org/licenses/GPL-3.0).

Got feedback? We'd love to hear from you! Email us @ cat {at} engagementlab {dot} org.