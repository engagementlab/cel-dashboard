# Require whichever elevator you're using below here...
#
# require 'apartment/elevators/generic'
# require 'apartment/elevators/domain'
# require 'apartment/elevators/subdomain'

#
# Apartment Configuration
#
Apartment.configure do |config|

  config.db_migrate_tenants = false

  # These models will not be multi-tenanted,
  # but remain in the global (public) namespace
  #
  # An example might be a Customer or Tenant model that stores each tenant information
  # ex:
  #
  # config.excluded_models = %w{Tenant}
  #
  config.excluded_models = %w{Client User SyncTable SyncLog SyncDetailLog SyncAkTableTemplate}

  # use postgres schemas?
  config.use_schemas = true

  # Also include the tenatn's AK schema and the public schemas
  config.persistent_schemas = lambda{|tenant| [tenant.gsub(/_cc\z/, '_ak'), 'public']}

  # supply list of database names for migrations to run on
  config.tenant_names = lambda{ Client.where("active = true AND cc_schema_name IS NOT NULL AND LENGTH(cc_schema_name) > 0").pluck(:cc_schema_name) }
end

##
# Elevator Configuration

# Rails.application.config.middleware.use 'Apartment::Elevators::Generic', lambda { |request|
#   # TODO: supply generic implementation
# }

# Rails.application.config.middleware.use 'Apartment::Elevators::Domain'

# Rails.application.config.middleware.use 'Apartment::Elevators::Subdomain'

##
# Rake enhancements so that db:migrate etc... also runs migrations on all tenants
# require 'apartment/tasks/enhancements'
