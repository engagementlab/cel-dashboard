# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140825181641) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pgrowlocks"

  create_table "browser_specs", force: true do |t|
    t.string "platform",    default: "", null: false
    t.string "os",          default: "", null: false
    t.string "browser",     default: "", null: false
    t.string "browser_ver", default: "", null: false
    t.string "screen",      default: "", null: false
    t.string "orientation", default: "", null: false
  end

  add_index "browser_specs", ["platform", "os", "browser", "browser_ver", "screen", "orientation"], name: "index_browser_specs_multi_col", unique: true, using: :btree

  create_table "clients", force: true do |t|
    t.boolean  "active",                      default: true,                         null: false
    t.boolean  "admin",                       default: false,                        null: false
    t.string   "name",                                                               null: false
    t.string   "ak_schema_name",                                                     null: false
    t.string   "cc_schema_name",                                                     null: false
    t.integer  "master_user_id"
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.string   "domain"
    t.string   "ak_domain"
    t.string   "time_zone",                   default: "Pacific Time (US & Canada)"
    t.float    "email_action_threshold",      default: 0.03
    t.float    "email_action_threshold_time", default: 1.0
    t.float    "page_significance_threshold", default: 0.1
    t.integer  "default_timeframe",           default: 14
  end

  add_index "clients", ["name"], name: "index_clients_on_name", unique: true, using: :btree

  create_table "roles", force: true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "sync_ak_table_templates", force: true do |t|
    t.string "table_name"
    t.string "sync_method"
    t.string "pkey"
    t.string "pkey_type"
  end

  create_table "sync_detail_log", force: true do |t|
    t.integer  "sync_log_id",             null: false
    t.string   "table_name",              null: false
    t.integer  "inserted",    default: 0
    t.integer  "updated",     default: 0
    t.integer  "copied",      default: 0
    t.integer  "deleted",     default: 0
    t.float    "duration"
    t.float    "src_dur"
    t.float    "dst_dur"
    t.datetime "created_at"
  end

  add_index "sync_detail_log", ["sync_log_id"], name: "index_sync_detail_log_on_sync_log_id", using: :btree

  create_table "sync_log", force: true do |t|
    t.string   "schema_name"
    t.integer  "pid"
    t.datetime "start_ts"
    t.datetime "end_ts"
    t.float    "duration"
    t.string   "status"
    t.string   "err_msg"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sync_log", ["created_at"], name: "index_sync_log_on_created_at", using: :btree
  add_index "sync_log", ["schema_name"], name: "index_sync_log_on_schema_name", using: :btree

  create_table "sync_tables", force: true do |t|
    t.string   "schema_name"
    t.string   "table_name"
    t.string   "pkey"
    t.string   "pkey_type"
    t.string   "sync_method",   default: "ignore"
    t.boolean  "from_template", default: false
    t.integer  "src_col_cnt"
    t.integer  "dst_col_cnt"
    t.string   "err_msg"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "sync",          default: true
  end

  add_index "sync_tables", ["schema_name", "table_name"], name: "index_sync_tables_on_schema_name_and_table_name", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.integer  "client_id",                                                     null: false
    t.string   "email",                  default: "",                           null: false
    t.string   "encrypted_password",     default: ""
    t.string   "name"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,                            null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,                            null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.string   "time_zone",              default: "Pacific Time (US & Canada)"
    t.boolean  "active",                 default: true,                         null: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
