class CreateEmailSummary < ActiveRecord::Migration
  def up
    create_table :email_action_summary do |t|
      t.integer   :mailing_id
      t.integer   :subject_id
      t.integer   :target_page_id
      t.integer   :email_domain_id
      t.integer   :user_source_id
      t.integer   :opens
      t.integer   :opens_unique
      t.integer   :clicks
      t.integer   :clicks_unique
      t.integer   :actions
      t.integer   :actions_forwarded
      t.integer   :new_members
      t.integer   :unsubs
      t.integer   :unsub_complaints
      t.integer   :unsub_bounces
      t.integer   :donations
      t.float     :revenue
      t.timestamp :max_opened_at
      t.timestamp :max_clicked_at
      t.timestamp :max_actioned_at
    end
    add_index :email_action_summary, [:mailing_id, :subject_id], :name => "index_email_action_summary_multicol"
    add_index :email_action_summary, [:email_domain_id], :name => "index_email_action_summary_email_domain_id"
    add_index :email_action_summary, [:user_source_id], :name => "index_email_action_summary_user_source_id"


    create_table :email_action_summary_by_period do |t|
      t.column    :period, 'timestamp with time zone', :null => false
      t.integer   :mailing_id
      t.integer   :subject_id
      t.integer   :target_page_id
      t.integer   :email_domain_id
      t.integer   :user_source_id
      t.integer   :opens
      t.integer   :clicks
      t.integer   :actions
      t.integer   :actions_forwarded
      t.integer   :new_members
      t.integer   :unsubs
      t.integer   :unsub_complaints
      t.integer   :unsub_bounces
      t.integer   :donations
      t.float     :revenue
    end
    add_index :email_action_summary_by_period, [:period, :mailing_id, :subject_id], :name => "index_email_action_summary_by_period_period_multicol"
    add_index :email_action_summary_by_period, [:email_domain_id], :name => "index_email_action_summary_by_period_email_domain_id"
    add_index :email_action_summary_by_period, [:user_source_id], :name => "index_email_action_summary_by_period_user_source_id"

    create_table :email_sent_summary do |t|
      t.integer   :mailing_id
      t.integer   :subject_id
      t.integer   :email_domain_id
      t.integer   :user_source_id
      t.integer   :sent
    end
    add_index :email_sent_summary, [:mailing_id, :subject_id, :email_domain_id, :user_source_id, :sent], :name => "index_email_sent_summary_period_mailing_subject"

    create_table :domain_users do |t|
      t.integer :domain_id
      t.integer :user_id
    end
    add_index :domain_users, [:domain_id, :user_id]
    add_index :domain_users, [:user_id, :domain_id]

    create_table :domains do |t|
      t.string    :domain,       :null => false
      t.integer   :email_count,  :default => 0
      t.integer   :action_count, :default => 0
      t.timestamp :created_at
    end
    add_index :domains, :domain, :unique => true
    add_index :domains, [:email_count, :id, :domain], order: {email_count: :desc} 

    create_table :user_source_users do |t|
      t.integer :user_source_id
      t.integer :user_id
    end
    add_index :user_source_users, [:user_source_id, :user_id]
    add_index :user_source_users, [:user_id, :user_source_id]

    create_table :user_sources do |t|
      t.string    :user_source,       :null => false
      t.integer   :email_count,  :default => 0
      t.integer   :action_count, :default => 0
      t.timestamp :created_at
    end
    add_index :user_sources, :user_source, :unique => true
    add_index :user_sources, [:email_count, :id, :user_source], order: {email_count: :desc} 

    create_table :list_size do |t|
      t.integer   :list_id,  :null => false
      t.integer   :mailable, :default => 0
    end

    create_table :list_size_by_period do |t|
      t.column    :period, 'timestamp with time zone', :null => false
      t.integer   :list_id,      :null => false
      t.integer   :subs,         :default => 0
      t.integer   :unsubs,       :default => 0
      t.integer   :total_subs,   :default => 0
      t.integer   :total_unsubs, :default => 0
      t.integer   :mailable,     :default => 0
    end
    add_index :list_size_by_period, [:period, :list_id], :unique => true

    create_table :mailing_max_events do |t|
      t.integer   :mailing_id
      t.timestamp :max_opened_at
      t.timestamp :max_clicked_at
      t.timestamp :max_actioned_at
    end
    add_index :mailing_max_events, :mailing_id, :unique => true

    execute("INSERT INTO domains (id, domain, created_at) values (0, 'other', now())")

    execute("INSERT INTO user_sources (id, user_source, created_at) values (0, 'other', now())")

    execute(%q{CREATE OR REPLACE VIEW top_email_domains AS 
                SELECT 0 AS id, 'other' AS domain, 0 AS email_count
                UNION
                (SELECT id, domain, email_count FROM domains ORDER BY email_count DESC LIMIT 49)})
    
    execute(%q{CREATE OR REPLACE VIEW top_user_sources AS 
                SELECT 0 AS id, 'other' AS user_source, 0 AS email_count
                UNION
                (SELECT id, user_source, email_count FROM user_sources ORDER BY email_count DESC LIMIT 49)})

    execute(%q{CREATE OR REPLACE VIEW mailing_max_events_view AS
                SELECT mailings.mailing_id,
                       CASE WHEN opens.max_opened_at     IS NULL THEN '1970-01-01 00:00:00' ELSE opens.max_opened_at     END AS max_opened_at,
                       CASE WHEN clicks.max_clicked_at   IS NULL THEN '1970-01-01 00:00:00' ELSE clicks.max_clicked_at   END AS max_clicked_at,
                       CASE WHEN actions.max_actioned_at IS NULL THEN '1970-01-01 00:00:00' ELSE actions.max_actioned_at END AS max_actioned_at
                 FROM
                      (
                        SELECT cm.id AS mailing_id
                          FROM core_mailing cm
                         WHERE cm.status in ('completed', 'stopped', 'died')
                           AND cm.started_at IS NOT NULL
                      ) AS mailings
                      LEFT OUTER JOIN
                      (
                        SELECT co.mailing_id, MAX(co.created_at) max_opened_at
                          FROM core_open co
                         GROUP BY  co.mailing_id
                      ) AS opens ON mailings.mailing_id = opens.mailing_id
                      LEFT OUTER JOIN
                      (
                        SELECT cc.mailing_id, MAX(cc.created_at) max_clicked_at
                          FROM core_click cc
                         GROUP BY cc.mailing_id
                      ) AS clicks ON mailings.mailing_id = clicks.mailing_id
                      LEFT OUTER JOIN
                      (
                        SELECT ca.mailing_id, MAX(ca.created_at) max_actioned_at
                          FROM core_action ca
                         GROUP BY ca.mailing_id
                      ) AS actions
                      ON mailings.mailing_id = actions.mailing_id})

  end


  def down
    execute("DROP VIEW IF EXISTS mailing_max_events_view")
    execute("DROP VIEW IF EXISTS top_email_domains")
    execute("DROP VIEW IF EXISTS top_user_sources")
    drop_table :email_sent_summary
    drop_table :email_action_summary_by_period
    drop_table :email_action_summary
    drop_table :domain_users
    drop_table :domains
    drop_table :user_source_users
    drop_table :user_sources
    drop_table :list_size
    drop_table :list_size_by_period
    drop_table :mailing_max_events
  end
end

