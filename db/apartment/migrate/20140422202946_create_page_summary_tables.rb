class CreatePageSummaryTables < ActiveRecord::Migration

  def up
    create_table :page_action_summary do |t|
      t.integer   :page_id
      t.integer   :mailing_id
      t.integer   :subject_id
      t.integer   :user_source_id
      t.integer   :action_source_id
      t.integer   :actions,         :default => 0
      t.integer   :new_members,     :default => 0
      t.integer   :new_subscribers, :default => 0
      t.float     :revenue,         :default => 0
      t.integer   :max_action_id,   :default => 0
    end
    add_index :page_action_summary, [:page_id, :mailing_id, :subject_id], :name => "index_page_action_summary_multicol"
    add_index :page_action_summary, [:user_source_id, :page_id]
    add_index :page_action_summary, [:action_source_id, :page_id]

    create_table :page_action_summary_by_period do |t|
      t.column  :period, 'timestamp with time zone', :null => false
      t.integer :page_id
      t.integer :mailing_id
      t.integer :subject_id
      t.integer :user_source_id
      t.integer :action_source_id
      t.integer :actions,         :default => 0
      t.integer :new_members,     :default => 0
      t.integer :new_subscribers, :default => 0
      t.float   :revenue,         :default => 0
    end
    add_index :page_action_summary_by_period, [:page_id, :mailing_id, :subject_id], :name => "index_page_action_summary_by_period_multicol1"
    add_index :page_action_summary_by_period, [:user_source_id, :page_id],          :name => "index_page_action_summary_by_period_multicol2"
    add_index :page_action_summary_by_period, [:action_source_id, :page_id],        :name => "index_page_action_summary_by_period_multicol3"
  end


  def down
    drop_table :page_action_summary
    drop_table :page_action_summary_by_period 
  end  

end
