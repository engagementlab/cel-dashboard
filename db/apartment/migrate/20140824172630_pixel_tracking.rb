class PixelTracking < ActiveRecord::Migration
  def up
    create_table :page_view_summary do |t|
      t.integer   :page_id,         :null => false
      t.integer   :mailing_id,      :null => false, :default => 0
      t.integer   :referrer_url_id, :null => false, :default => 0
      t.integer   :browser_spec_id, :null => false, :default => 0
      t.integer   :views,           :null => false, :default => 0
      t.timestamp :updated_at
    end
    add_index :page_view_summary, [:page_id]

    create_table :page_view_summary_by_period do |t|
      t.column  :period, 'timestamp with time zone', :null => false
      t.integer :page_id,                            :null => false
      t.integer :mailing_id,                         :null => false, :default => 0
      t.integer :referrer_url_id,                    :null => false, :default => 0
      t.integer :browser_spec_id,                    :null => false
      t.integer :views,                              :null => false, :default => 0
    end
    add_index :page_view_summary_by_period, 
      [:page_id, :period, :mailing_id, :referrer_url_id, :browser_spec_id], 
      unique: true, 
      name: 'index_page_view_summary_by_period_multi_col'

    create_table :referrer_urls, :force => true do |t|
      t.string   :url,        :limit => 1024, :null => false
      t.datetime :created_at,                 :null => false
    end
    add_index :referrer_urls, [:url]   #, :name => "index_referrer_urls_on_url"
  end


  def down
    drop_table :page_view_summary
    drop_table :page_view_summary_by_period
    drop_table :referrer_urls
  end
end
