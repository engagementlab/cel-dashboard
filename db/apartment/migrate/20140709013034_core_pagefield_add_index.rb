class CorePagefieldAddIndex < ActiveRecord::Migration
  def up
    execute(%q{DROP INDEX IF EXISTS index_core_pagefield_parent_id_name })
    execute(%q{CREATE INDEX index_core_pagefield_parent_id_name ON core_pagefield (parent_id, name) })
  end

  def down
    execute(%q{DROP INDEX IF EXISTS index_core_pagefield_parent_id_name })
  end
end
