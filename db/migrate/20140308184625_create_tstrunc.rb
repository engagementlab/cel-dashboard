#
# Postgresql convenience functions to aid summary table population
#
# The built-in Postgresql function date_trunc() only truncates
# to whole intervals such as minute, hour, day. For instance, 
# date_trunc() can truncate to exactly one minute or one hour,
# but not 5 minutes or 2 hours.
#
#     http://www.postgresql.org/docs/9.1/static/functions-datetime.html#FUNCTIONS-DATETIME-TRUNC
#
# tstrunc() is similar to date_trunc() but it allows arbitrary intervals. Examples
#
#     tstrunct(now(), '10 seconds')
#     tstrunct(now(), '5 minutes')
#     tstrunct(now(), '3 hours')
#
# Additional convenience functions exist for common operations
#
#     tstrunc_5m(now())   ==> tstrunc(now(), '5 minutes')
#     tstrunc_10m(now())  ==> tstrunc(now(), '10 minutes')
#     tstrunc_15m(now())  ==> tstrunc(now(), '15 minutes')
#     tstrunc_30m(now())  ==> tstrunc(now(), '30 minutes')
#
# These functions were inspired by http://wiki.postgresql.org/wiki/Round_time.
# However, the example at the previous link *round* the timestamp 
# as opposed to *truncating* it.
#
class CreateTstrunc < ActiveRecord::Migration
  def up
    sql = %q{ CREATE OR REPLACE FUNCTION tstrunc(ts timestamptz, round_interval INTERVAL) RETURNS timestamp AS $BODY$
              SELECT TIMESTAMP WITHOUT TIME ZONE 'epoch' + (EXTRACT(epoch FROM $1)::INTEGER)
                              / EXTRACT(epoch FROM $2)::INTEGER * EXTRACT(epoch FROM $2)::INTEGER * INTERVAL '1 second';
              $BODY$ LANGUAGE SQL STABLE;
    }
    execute(sql)

    sql = %q{ CREATE OR REPLACE FUNCTION tstrunc_5m(ts timestamptz) RETURNS timestamp AS $BODY$
              SELECT TIMESTAMP WITHOUT TIME ZONE 'epoch' + (EXTRACT(epoch FROM $1)::INTEGER) / 300 * 300 * INTERVAL '1 second';
              $BODY$ LANGUAGE SQL STABLE;
    }
    execute(sql)

    sql = %q{ CREATE OR REPLACE FUNCTION tstrunc_10m(ts timestamptz) RETURNS timestamp AS $BODY$
              SELECT TIMESTAMP WITHOUT TIME ZONE 'epoch' + (EXTRACT(epoch FROM $1)::INTEGER) / 600 * 600 * INTERVAL '1 second';
              $BODY$ LANGUAGE SQL STABLE;
    }
    execute(sql)

    sql = %q{ CREATE OR REPLACE FUNCTION tstrunc_15m(ts timestamptz) RETURNS timestamp AS $BODY$
              SELECT TIMESTAMP WITHOUT TIME ZONE 'epoch' + (EXTRACT(epoch FROM $1)::INTEGER) / 900 * 900 * INTERVAL '1 second';
              $BODY$ LANGUAGE SQL STABLE;
    }
    execute(sql)

    sql = %q{ CREATE OR REPLACE FUNCTION tstrunc_30m(ts timestamptz) RETURNS timestamp AS $BODY$
              SELECT TIMESTAMP WITHOUT TIME ZONE 'epoch' + (EXTRACT(epoch FROM $1)::INTEGER) / 1800 * 1800 * INTERVAL '1 second';
              $BODY$ LANGUAGE SQL STABLE;
    }
    execute(sql)
  end

  def down
    execute(%q{DROP FUNCTION IF EXISTS tstrunc(timestamptz, interval)})
    execute(%q{DROP FUNCTION IF EXISTS tstrunc_5m(timestamptz)})
    execute(%q{DROP FUNCTION IF EXISTS tstrunc_10m(timestamptz)})
    execute(%q{DROP FUNCTION IF EXISTS tstrunc_15m(timestamptz)})
    execute(%q{DROP FUNCTION IF EXISTS tstrunc_30m(timestamptz)})
  end
end

