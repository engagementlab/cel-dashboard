class ClientsAndUsersAddTimezone < ActiveRecord::Migration
  def change
    add_column :clients, :time_zone, :string, :default => 'Pacific Time (US & Canada)'
    add_column :users,   :time_zone, :string, :default => 'Pacific Time (US & Canada)'
  end
end
