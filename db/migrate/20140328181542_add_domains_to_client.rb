class AddDomainsToClient < ActiveRecord::Migration
  def change
    add_column :clients, :domain,    :string
    add_column :clients, :ak_domain, :string
  end
end
