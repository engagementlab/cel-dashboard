class RedoClientConfig < ActiveRecord::Migration

  def up
    change_table :clients do |t|
      t.remove  :config
      t.float   :email_action_threshold,      :default => 0.03
      t.float   :email_action_threshold_time, :default => 1     # measured in hours
      t.float   :page_significance_threshold, :default => 0.10
      t.integer :default_timeframe,           :default => 14    # measures in days
    end
  end

  def down
    remove_column :clients, :email_action_threshold
    remove_column :clients, :email_action_threshold_time
    remove_column :clients, :page_significance_threshold
    remove_column :clients, :default_timeframe

    add_column :clients, :config, :json, :default => {}
  end
end
