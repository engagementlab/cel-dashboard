class BrowserSpecs < ActiveRecord::Migration
  def change
    create_table :browser_specs do |t|
      t.string  :platform,    :null => false, :default => ''   # desktop, tablet, mobile
      t.string  :os,          :null => false, :default => ''
      t.string  :browser,     :null => false, :default => ''
      t.string  :browser_ver, :null => false, :default => ''
      t.string  :screen,      :null => false, :default => ''
      t.string  :orientation, :null => false, :default => ''
    end
    add_index :browser_specs, [:platform, :os, :browser, :browser_ver, :screen, :orientation],  unique: true,  name: 'index_browser_specs_multi_col'
  end
end
