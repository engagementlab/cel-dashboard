require 'csv'

# -------------------------------
puts "Loading AK table template"

SyncAkTableTemplate.connection.execute('TRUNCATE sync_ak_table_templates RESTART IDENTITY')
template_file = File.read("#{Rails.root}/db/sync_ak_tables_template-data.csv")
csv = CSV.parse(template_file, :headers => true)
csv.each do |row|
  SyncAkTableTemplate.create!(row.to_hash)
end
