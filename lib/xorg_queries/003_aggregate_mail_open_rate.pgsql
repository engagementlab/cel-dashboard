SELECT SUM(sent) AS "Total sent",
       AVG(sent) AS "Avg send",
       SUM(opened) AS "Total open",
       AVG(opened) AS "Avg open",
       (SUM(opened) / SUM(sent)) AS "Open rate",
       AVG(rate)   AS "Avg open rate"
  FROM
       (
        SELECT cm.id mailing_id, 
               cm.expected_send_count AS sent,
               opened.cnt AS opened,
               ROUND(opened.cnt / cm.expected_send_count * 100, 2) AS rate
          FROM core_mailing cm,
               (
                 SELECT count(1) AS cnt, co.mailing_id
                   FROM core_open co
                  GROUP by co.mailing_id
               ) AS opened
         WHERE cm.id = opened.mailing_id
           AND cm.status = 'completed'
       ) AS opens
;