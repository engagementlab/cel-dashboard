SELECT SUM(sent)   AS "Total sent",
       AVG(sent)   AS "Avg send",
       SUM(unsubs) AS "Total unsubs",
       AVG(unsubs) AS "Avg unsubs",
       (SUM(unsubs) / SUM(sent)) AS "Unsub rate",
       AVG(unsub_rate) AS "Avg unsub rate"
  FROM
       (
        SELECT cm.id mailing_id, 
               cm.expected_send_count AS sent,
               xunsubs.unsubs,
               ROUND(xunsubs.unsubs / cm.expected_send_count * 100, 2) AS unsub_rate
          FROM core_mailing cm,
               (
                SELECT um.mailing_id,  
                      COUNT(1) as unsubs
                  FROM core_mailing as m, core_action a, core_usermailing um, core_unsubscribeaction us, core_subscriptionhistory sh
                 WHERE m.id = um.mailing_id
                   AND um.mailing_id = a.mailing_id
                   AND um.user_id = a.user_id
                   AND a.id = us.action_ptr_id
                   AND a.id = sh.action_id
                 GROUP BY um.mailing_id
               ) AS xunsubs
         WHERE cm.id = xunsubs.mailing_id
           AND cm.status = 'completed'
       ) AS unsubs
;
