SELECT SUM(mailings)                 AS total_mailings,
       ROUND(AVG(mailings), 2)       AS avg_mailings,
       SUM(total_recipients)         AS total_recipients,
       ROUND(AVG(avg_recipients), 2) AS avg_recipients
  FROM
       (
        SELECT DATE_PART('year', created_at) AS year,
               DATE_PART('week', created_at) AS week,
               COUNT(1)                      AS mailings,
               SUM(expected_send_count)      AS total_recipients,
               AVG(expected_send_count)      AS avg_recipients
          FROM core_mailing
         WHERE status = 'completed'
         GROUP BY 1, 2
         ORDER BY 1, 2
       ) AS detail
;
