-- Average number of actions per member by action type
--
-- Note: Import and Unsubscribe actions are NOT counted
--

SELECT page_type AS action_type, AVG(actions) AS avg_actions
  FROM
      (
        SELECT user_id, page_type, SUM(action) AS actions
          FROM 
              (
                SELECT cu.id AS user_id, 
                       CASE WHEN cp.type IS NULL THEN '--none--' ELSE cp.type END AS page_type,
                       CASE WHEN ca.id IS NULL THEN 0 ELSE 1 END AS action
                  FROM 
                       core_user cu
                       LEFT OUTER JOIN core_action ca ON cu.id = ca.user_id AND ca.status = 'complete'
                       LEFT OUTER JOIN core_page cp ON ca.page_id = cp.id AND cp.type NOT IN ('Import', 'Unsubscribe')
                 WHERE ca.id is NULL 
                    OR (ca.status = 'complete' AND cp.type NOT IN ('Import', 'Unsubscribe') )
              ) as X
         GROUP BY user_id, page_type
      ) as Y
 GROUP BY page_type
;