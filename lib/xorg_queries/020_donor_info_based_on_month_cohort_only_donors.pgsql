SELECT detail.months, 
       new_members.cnt AS new_members,
       AVG(donations) AS avg_donations, STDDEV(donations) AS stddev_donations,
       SUM(total_donations) AS total_donations,
       AVG(total_donations) AS avg_total_donations,
       AVG(avg_donation) AS avg_avg_donation
  FROM 
      (
        SELECT cu.id AS user_id, 
               CASE WHEN donations.cnt IS NULL THEN 0 ELSE donations.cnt END AS donations,
               donations.total AS total_donations,
               donations.avg AS avg_donation,
               (DATE_PART('year', now()) - DATE_PART('year', cu.created_at)) * 12 + (DATE_PART('month', now()) - DATE_PART('month', cu.created_at)) months
          FROM core_user cu
               LEFT OUTER JOIN
              (
               SELECT co.user_id, COUNT(1) AS cnt, SUM(total) AS total, AVG(total) AS avg
                 FROM core_order AS co, core_transaction ct
                WHERE co.id = ct.order_id
                  AND co.total > 0
                  AND ct.status = 'completed'
                GROUP by co.user_id
              ) AS donations ON cu.id = donations.user_id
        WHERE donations.cnt IS NOT NULL
      ) AS detail,
      (
       SELECT COUNT(1) AS cnt,
              (DATE_PART('year', now()) - DATE_PART('year', cu.created_at)) * 12 + (DATE_PART('month', now()) - DATE_PART('month', cu.created_at)) months
         FROM core_user AS cu
        GROUP BY months
      ) AS new_members
 WHERE new_members.months = detail.months
 GROUP BY detail.months, new_members
 ORDER BY detail.months
;
