SELECT DATE_PART('year', created_at) AS day,
       COUNT(1)                      AS mailings,
       SUM(expected_send_count)      AS total_recipients,
       AVG(expected_send_count)      AS avg_recipients
  FROM core_mailing
 WHERE status = 'completed'
 GROUP BY 1
 ORDER BY 1
;
