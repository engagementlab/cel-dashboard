SELECT AVG(total) AS avg_donation, STDDEV(total) AS stddev_donation
  FROM core_order co, core_transaction ct
 WHERE co.id = ct.order_id 
   AND ct.status = 'completed'
   AND co.total > 0
;
