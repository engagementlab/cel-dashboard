SELECT num_emails,
       COUNT(1) as members
  FROM
       (
        SELECT emails.user_id, 
               SUM(emails.rcvd_email) AS num_emails
          FROM
               (
                SELECT user_detail.user_id,
                       CASE WHEN cm.user_id IS NULL THEN 0 ELSE 1 END rcvd_email
                  FROM 
                       (
                        SELECT cu.id user_id, cu.created_at user_created_at, ca.created_at action_created_at
                          FROM core_user cu, core_action ca,
                               (
                                 SELECT MIN(cax.id) AS id
                                   FROM core_action cax
                                  WHERE cax.created_user = false
                                    AND cax.status = 'complete'
                                  GROUP BY cax.user_id
                               ) AS first_action
                         WHERE first_action.id = ca.id
                           AND cu.id = ca.user_id
                       ) as user_detail
                       LEFT OUTER JOIN core_usermailing cm ON user_detail.user_id = cm.user_id
                 WHERE (cm.created_at > user_detail.user_created_at AND cm.created_at < user_detail.action_created_at)
                    OR cm.user_id IS NULL
               ) AS emails
         GROUP BY emails.user_id
       ) AS detail
 GROUP BY num_emails
;