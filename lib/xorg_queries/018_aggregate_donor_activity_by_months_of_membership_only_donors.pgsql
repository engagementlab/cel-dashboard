SELECT months, AVG(donations) AS avg_donations, STDDEV(donations) AS stddev_donations
  FROM 
      (
        SELECT cu.id AS user_id, 
               CASE WHEN donations.cnt IS NULL THEN 0 ELSE donations.cnt END AS donations,
               (DATE_PART('year', now()) - DATE_PART('year', cu.created_at)) * 12 + (DATE_PART('month', now()) - DATE_PART('month', cu.created_at)) months
          FROM core_user cu
               LEFT OUTER JOIN
              (
               SELECT co.user_id, COUNT(1) AS cnt
                 FROM core_order AS co, core_transaction ct
                WHERE co.id = ct.order_id
                  AND co.total > 0
                  AND ct.status = 'completed'
                GROUP by co.user_id
              ) AS donations ON cu.id = donations.user_id
        WHERE donations.cnt IS NOT NULL
      ) AS detail
 GROUP BY months
 ORDER BY months
;
