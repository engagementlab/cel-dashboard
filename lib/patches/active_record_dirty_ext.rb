module Ext
  module ActiveRecord
    module Dirty
  private

  # Private: Overwrites the default implementation to include arrays and hstores
  # in the list of attributes that should always be saved.
  #
  # Without this, arrays and stores don't get their values updated in
  # the database.
  #
  # Relates to this issue: https://github.com/rails/rails/issues/6127
  #def keys_for_partial_write
  #  super | self.class.columns.select do |c|
  #    c.try(:array) || c.type == :hstore || c.type == :json
  #  end.map(&:name)
  #end

    end
  end
end

ActiveSupport.on_load :active_record do
  include Ext::ActiveRecord::Dirty
end
