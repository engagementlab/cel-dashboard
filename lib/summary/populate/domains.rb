module Summary
  module Populate
    class << self

  def domains_from_user_emails(client)
    #
    # Note: The "WHERE POSITION(' ' IN email) = 0" is required
    # to eliminate bad data. Some core_user.email fields actually 
    # have two email address, separated by ', '. wtf...
    #
    sql = %Q{ INSERT INTO #{client.cc_schema}.domains (domain, created_at)
              SELECT ud.domain, LOCALTIMESTAMP
                FROM 
                    (
                     SELECT distinct LOWER(SUBSTRING(email FROM POSITION('@' IN email)+1)) AS domain
                       FROM #{client.ak_schema}.core_user
                      WHERE POSITION(' ' IN email) = 0
                    ) AS ud
                    LEFT OUTER JOIN #{client.cc_schema}.domains ed ON ud.domain = ed.domain
                    WHERE ed.domain IS NULL
    }

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
      Apartment.connection.execute("ANALYZE #{client.cc_schema}.domains")
    end
  end


  def domain_users(client)
    #
    # Note: The "WHERE POSITION(' ' IN email) = 0" is required
    # to eliminate bad data. Some core_user.email fields actually 
    # have two email address, separated by ', '. wtf...
    #
    sql = %Q{ INSERT INTO #{client.cc_schema}.domain_users (domain_id, user_id)
              SELECT d.id, cu.id 
                FROM #{client.cc_schema}.domains d,
                     #{client.ak_schema}.core_user cu
                     LEFT OUTER JOIN #{client.cc_schema}.domain_users du ON cu.id = du.user_id
               WHERE POSITION(' ' IN cu.email) = 0
                 AND du.user_id IS NULL
                 AND d.domain = LOWER(SUBSTRING(cu.email FROM POSITION('@' IN cu.email)+1))
    }

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
      Apartment.connection.execute("ANALYZE #{client.cc_schema}.domain_users")
    end
  end


  def domain_email_counts(client)
    sql = %Q{ UPDATE #{client.cc_schema}.domains AS d
                 SET email_count = emails.cnt FROM (SELECT domain_id, count(1) AS cnt
                                                      FROM #{client.cc_schema}.domain_users 
                                                     GROUP BY domain_id) AS emails
               WHERE d.id = emails.domain_id;
    }

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
    end
  end

    end
  end
end