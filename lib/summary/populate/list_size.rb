module Summary
  module Populate
    class << self

  def list_size(client)
    sql1 = %Q{ TRUNCATE #{client.cc_schema}.list_size RESTART IDENTITY }

    sql2 = %Q{
              INSERT INTO #{client.cc_schema}.list_size (list_id, mailable)
              SELECT list_id,
                     COUNT(1) AS mailable
                FROM #{client.ak_schema}.core_subscription
               GROUP BY list_id;
    }

    max_period = nil
    Apartment::Database.process(client.cc_schema) do

      ActiveRecord::Base.transaction do
        Apartment.connection.execute(sql1)
        Apartment.connection.execute(sql2)
      end
    end
  end

    end
  end
end
