module Summary
  module Populate
    class << self

  private

  def page_ids(client)
    sql = %Q{  SELECT DISTINCT id from core_page ORDER BY id}

    page_ids = []
    Apartment::Database.process(client.cc_schema) do
      page_ids = Apartment.connection.execute(sql).collect{|r| r['id'].to_i}
    end
    page_ids
  end


  def page_action_summary_by_period_chunk(client, type, data)

    if type == 'page'
      actions_where = "ca.page_id = #{data}"
    else
      actions_where = "ca.created_at >= '#{data}'"
    end

    sql = %Q{ INSERT INTO page_action_summary_by_period (period,
                                                         page_id,
                                                         mailing_id, subject_id,
                                                         user_source_id, action_source_id,
                                                         actions,
                                                         new_members, new_subscribers,
                                                         revenue)
              SELECT tstrunc_5m(ca.created_at) AS period,
                     ca.page_id, 
                     ca.mailing_id, um.subject_id,
                     CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS user_source_id,
                     CASE WHEN tas.id IS NULL THEN 0 ELSE tas.id END AS action_source_id,
                     COUNT(1) AS actions,
                     SUM(CASE WHEN ca.created_user    = TRUE THEN 1 ELSE 0 END)        AS new_members,
                     SUM(CASE WHEN ca.subscribed_user = TRUE THEN 1 ELSE 0 END)        AS new_subscribers,
                     SUM(CASE WHEN co.action_id      IS NULL THEN 0 ELSE co.total END) AS revenue
                FROM core_action ca
                     LEFT OUTER JOIN core_order co ON ca.id = co.action_id AND co.status = 'completed'
                     LEFT OUTER JOIN core_usermailing um ON ca.mailing_id = um.mailing_id AND ca.user_id = um.user_id
                     LEFT OUTER JOIN top_action_sources tas ON tas.source = ca.source,
                     source_users su
                     LEFT OUTER JOIN top_user_sources tus ON tus.id = su.source_id
               WHERE ca.status = 'complete'
                 AND ca.user_id = su.user_id
                 AND #{actions_where}
               GROUP BY period, ca.page_id, ca.mailing_id, um.subject_id, tus.id, tas.id
               ORDER BY period, ca.page_id, ca.mailing_id, um.subject_id
    }

    Apartment::Database.process(client.cc_schema) do
      result = Apartment.connection.execute(sql)
      puts "type= #{type}  data= #{data}  rows inserted= #{result.cmd_tuples}" if Rails.env == 'development'
    end
  end


  public


  #
  # The latest AK sync will almost certainly have additional data
  # for the last period in the summary table. The easist way to 
  # ensure that the last period in summary table is complete is
  # to delete it and re-populate.
  #
  def page_action_summary_by_period(client)
    sql1 = %Q{ SELECT MAX(period) max_period FROM #{client.cc_schema}.page_action_summary_by_period }

    sql2 = %Q{ DELETE FROM #{client.cc_schema}.page_action_summary_by_period WHERE period = ? }

    max_period = nil
    Apartment::Database.process(client.cc_schema) do
      max_period = Apartment.connection.execute(sql1).first['max_period']

      if max_period

        ActiveRecord::Base.transaction do
          sql2 = ActiveRecord::Base.escape_sql([sql2, max_period])
          result = Apartment.connection.execute(sql2)
          puts "rows deleted= #{result.cmd_tuples}" if Rails.env == 'development'
          page_action_summary_by_period_chunk(client, 'timestamp', max_period)
        end

      else

        page_ids(client).each do |id|
          page_action_summary_by_period_chunk(client, 'page', id)
        end
      end
    end
  end



    end
  end
end
