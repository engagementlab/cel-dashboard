module Summary
  module Query
    class << self

  def page_sources(client, page_id, source_type, limit = 50, json = false)

    raise "Unknown source_type: #{source_type}" if ! ['action', 'user'].include?(source_type)
      
    sql = %Q{
             SELECT s.id AS source_id,
                    s.source,
                    SUM(pas.actions) AS actions,
                    SUM(pas.new_members) AS new_members,
                    SUM(pas.new_subscribers) AS new_subscribers,
                    SUM(revenue) AS revenue
               FROM page_action_summary pas,
                    top_#{source_type}_sources s
              WHERE pas.#{source_type}_source_id = s.id
                AND pas.page_id = ?
              GROUP BY s.id, s.source
              ORDER BY SUM(pas.actions) DESC
              LIMIT ?
            }

    sql = ActiveRecord::Base.escape_sql([sql, page_id, limit])

    rows = nil
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    return rows if json == false

    Yajl::Encoder.encode(rows.to_a)
  end

    end
  end
end
