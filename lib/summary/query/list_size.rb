module Summary
  module Query
    class << self

  def list_size(client, json = false)

    sql = %Q{ SELECT ls.list_id, cl.is_default, cl.name, ls.mailable
                FROM #{client.cc_schema}.list_size ls,
                     #{client.ak_schema}.core_list cl
               WHERE ls.list_id = cl.id
               ORDER BY ls.mailable DESC
    }

    rows = []
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    return rows if json == false

    Yajl::Encoder.encode(rows.to_a)
  end

    end
  end
end
