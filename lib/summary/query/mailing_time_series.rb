module Summary
  module Query
    class << self

      # params:
      #   - mailing_ids => "1,2,3" or [mailing_id1, mailing_id2, ...]
      #   - activity_start_dt => dt
      #   - activity_end_dt => dt
      #   - domain_ids => "1,2,3" or [d1, d2, d3]
      #   - subject_idss => "1,2,3" or [s1, s2, s3...]
      #   - domain_keys => boolean
      #
      # duration => periods
      #   dur <= 2 weeks => minute
      #   dur <= 3 months => hour
      #   dur <= 2 year => day
      #   dur > 2 years => week
      #
      def mailing_time_series(client, params, json: true, tz: 'UTC', period: nil, interval: nil)
        return [] if client.nil? || client.cc_schema.blank?

        params = params.dup
        params[:mailing_start_dt] = 2.weeks.ago if params.empty?
        params[:mailing_ids] = params[:mailing_id] if params[:mailing_id].present? && params[:mailing_ids].blank?


        mailing_clause = "es.mailing_id"
        mailing_where = "mailing_id IN (#{param_for_sql_in(params[:mailing_ids])})"
        mailing_group = "es.mailing_id,"


        subject_where = ""
        if params[:subject_ids].present?
          subject_where = "AND es.subject_id IN (#{param_for_sql_in(params[:subject_ids])}) "
        end

        domain_where = ""
        domain_clause = ""
        domain_eas_clause = ""
        domain_table = ""
        if params[:domain_ids].present?
          domain_clause = "d.id, d.domain,"
          domain_eas_clause = "eas.id AS domain_id, eas.domain,"
          domain_where = "AND d.id IN (#{param_for_sql_in(params[:domain_ids])}) AND es.email_domain_id = d.id"
          domain_table = ",top_email_domains d"
        end

        user_source_where = ""
        if params[:user_source_ids].present?
          user_source_where = "AND es.user_source_id IN (#{param_for_sql_in(params[:user_source_ids])}) "
        end

        tags_table = ""
        tags_where = ""
        if params[:tag_ids].present?
          tags_table = ",core_mailing_tags cmt"
          tags_where = "AND cmt.tag_id IN (#{param_for_sql_in(params[:tag_ids])}) AND cmt.mailing_id = es.mailing_id "
        end

        if params[:activity_start_dt].present? || params[:activity_end_dt].present?
          if params[:activity_start_dt].present?
            sdt = Time.parse(params[:activity_start_dt]) rescue 2.weeks.ago
          else
            sdt = 2.weeks.ago
          end
          if params[:activity_end_dt].present?
            edt = Time.parse(params[:activity_end_dt]) rescue Time.now.utc
          else
            edt = Time.now.utc
          end
        else
          if params[:mailing_ids].present? || params[:tag_ids].present? || params[:mailing_start_dt].present? || params[:mailing_end_dt].present?
            sdt, edt = dates_from_mailing_data(client, params, period)
          else
            sdt = 2.weeks.ago
            edt = Time.now.utc
          end
        end

        if params[:period]
          time_unit = params[:period]
        else
          if sdt && edt
            tdiff = edt - sdt
            if tdiff < 1.day
              time_unit = 'minute'
            elsif tdiff <= 1.month + 10.seconds
              # Believe it or not, when no start_dt is passed, the
              # brief delay between setting sdt and etd is sufficient
              # to create a difference that is a fraction of a second
              # greater than 1 month. Thus the "+ 10.seconds". Sigh...
              time_unit = 'hour'
            elsif tdiff <= 2.years
              time_unit = 'day'
            else
              time_unit = 'week'
            end
          else
            time_unit = 'day'
          end
        end

        activity_dt_where = ""
        activity_dt_where << "AND es.period >= '#{sdt.to_s(:db)}' " if sdt
        activity_dt_where << "AND es.period < '#{edt.to_s(:db)}' "  if edt


        if interval.nil?
          period_clause = "CAST(DATE_TRUNC('#{time_unit}', es.period AT TIME ZONE '#{tz}') AS TIMESTAMP WITHOUT TIME ZONE)"
          period_epoch_clause = "CAST(EXTRACT(EPOCH from DATE_TRUNC('#{time_unit}', es.period AT TIME ZONE '#{tz}')) as bigint) * 1000::bigint"
        else
          period_clause = "CAST(tstrunc( es.period AT TIME ZONE '#{tz}', '#{interval} seconds') AS TIMESTAMP WITHOUT TIME ZONE)"
          period_epoch_clause = "CAST(EXTRACT(EPOCH from tstrunc( es.period AT TIME ZONE '#{tz}', '#{interval} seconds')) as bigint) * 1000::bigint"
        end

        sql = %Q{
                  SELECT eas.mailing_id, #{domain_eas_clause}
                  eas.period, eas.period_epoch, ess.sent, eas.opens, eas.clicks, eas.actions, eas.new_members, eas.unsubs, eas.unsub_complaints, eas.unsub_bounces
                    FROM
                         (
                           SELECT #{mailing_clause},
                                  #{domain_clause}
                                  #{period_clause}  AS period,
                                  #{period_epoch_clause} AS period_epoch,
                                  SUM(es.opens) AS opens,
                                  SUM(es.clicks) AS clicks,
                                  SUM(es.actions) AS actions,
                                  SUM(es.new_members) AS new_members,
                                  SUM(es.unsubs) AS unsubs,
                                  SUM(es.unsub_complaints) as unsub_complaints,
                                  SUM(es.unsub_bounces) as unsub_bounces
                             FROM email_action_summary_by_period es
                                  #{tags_table}                     
                                  #{domain_table}         
                            WHERE #{mailing_where}
                                  #{activity_dt_where}
                                  #{subject_where}
                                  #{domain_where}
                                  #{user_source_where}
                                  #{tags_where}
                         GROUP BY #{mailing_group} 
                                  #{period_clause}, 
                                  #{domain_clause} 
                                  period_epoch
                         ) AS eas,
                         (
                           SELECT mailing_id, SUM(sent) AS sent
                             FROM email_sent_summary
                            WHERE #{mailing_where}                                  
                                  #{user_source_where}
                            GROUP BY mailing_id
                         ) AS ess
                   WHERE eas.mailing_id = ess.mailing_id
                   ORDER BY eas.mailing_id, eas.period_epoch
        }

        rows = nil
        Apartment::Database.process(client.cc_schema) do
          rows = Apartment.connection.execute(sql)
        end

        return rows if json == false

        data = {}
        rows.each do |r|
          if params[:domain_keys]
            id = r['domain_id']
          else
            id = r['mailing_id']
          end
          data[id] = [] if data[id].nil?
          for event in ['opens', 'clicks', 'actions', 'unsubs', 'unsub_complaints', 'unsub_bounces']
            event = 'complaints' if event == 'unsub_complaints'
            event = 'bounces' if event == 'unsub_bounces'
            data[id] << {
              :mailing_id   => r['mailing_id'].to_i,
              :domain       => r['domain'],
              :domain_id    => r['domain_id'],
              :period       => r['period'],
              :period_epoch => r['period_epoch'].to_i,
              :event        => event,
              :count        => r[event].to_i
            }
          end
        end

        Yajl::Encoder.encode(data)
      end


      private


      def dates_from_mailing_data(client, params, period)
        # sdt = 2.weeks.ago
        # edt = Time.now.utc
        sdt = edt = nil

        summary_table = ""

        mailings_where = ""
        mailings_where = "AND cm.id IN (#{param_for_sql_in(params[:mailing_ids])})" if params[:mailing_ids].present?

        mailing_dt_where = ""
        if params[:mailing_start_dt].present? || params[:mailing_end_dt].present?
          summary_table = " email_action_summary_by_period es, "
          summary_where = "AND es.mailing_id = cm.id "
          mailing_dt_where << "AND cm.started_at >= '#{params[:mailing_start_dt]}' AND es.period >= '#{params[:mailing_start_dt]}' " if params[:mailing_start_dt].present?
          mailing_dt_where << "AND cm.started_at < '#{params[:mailing_end_dt]}' " if params[:mailing_end_dt].present?
        end

        activity_dt_where = ""
        if params[:activity_start_dt].present? || params[:activity_end_dt].present?
          summary_table = " email_action_summary_by_period es, "
          summary_where = "AND es.mailing_id = cm.id "
          activity_dt_where << "AND cm.id = es.mailing_id "
          activity_dt_where << "AND es.period >= '#{params[:activity_start_dt]}' " if params[:activity_start_dt].present?
          activity_dt_where << "AND es.period < '#{params[:activity_end_dt]}' "    if params[:activity_end_dt].present?
        end

        tags_table = ""
        tags_where = ""
        if params[:tag_ids].present?
          tags_table = "core_mailing_tags cmt,"
          tags_where = "AND cmt.tag_id IN (#{param_for_sql_in(params[:tag_ids])}) AND cmt.mailing_id = cm.id "
        end

        sql = %Q{ SELECT MIN(cm.started_at) AS sdt,
                         MAX(cm.started_at) AS edt
                    FROM #{summary_table}
                         #{tags_table}
                         core_mailing cm
                   WHERE 1 = 1
                         #{mailings_where}
                         #{summary_where}
                         #{mailing_dt_where}
                         #{activity_dt_where}
                         #{tags_where}
                   LIMIT 1
        }

        rows = nil
        Apartment::Database.process(client.cc_schema) do
          rows = Apartment.connection.execute(sql)
        end

        if rows
          if rows[0] && rows[0]['sdt']
            sdt = Time.parse(rows[0]['sdt'])
            sdt = Time.parse(params[:mailing_start_dt].to_s) if params[:mailing_start_dt].present? && sdt > Time.parse(params[:mailing_start_dt].to_s)
          end

          if rows[0] && rows[0]['edt']
            edt = Time.parse(rows[0]['edt']) + (period != nil ? period.days : 3.days)
            edt = Time.now.utc if edt > Time.now.utc
          end
        end

        [sdt, edt]
      end

    end
  end
end
