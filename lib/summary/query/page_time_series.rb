module Summary
  module Query
    class << self

      # params:
      #   - page_ids => "1,2,3" or [page_id1, page_id2, ...]
      #   - activity_start_dt => dt
      #   - activity_end_dt => dt
      #   - domain_ids => "1,2,3" or [d1, d2, d3]
      #   - subject_idss => "1,2,3" or [s1, s2, s3...]
      #
      # duration => periods
      #   dur <= 2 weeks => minute
      #   dur <= 3 months => hour
      #   dur <= 2 year => day
      #   dur > 2 years => week
      #
  def page_time_series(client, params, json: true, tz: 'UTC', period: nil, interval: nil, order: 'ASC')
    return [] if client.nil? || client.cc_schema.blank?

    params = params.dup
    params[:page_ids] = params[:page_id] if params[:page_id].present? && params[:page_ids].blank?

    page_where = "page_id IN (#{param_for_sql_in(params[:page_ids])})"

    user_source_where = ""
    if params[:user_source_ids].present?
      user_source_where = "AND pas.user_source_id IN (#{param_for_sql_in(params[:user_source_ids])}) "
    end

    action_source_where = ""
    if params[:action_source_ids].present?
      action_source_where = "AND pas.action_source_id IN (#{param_for_sql_in(params[:action_source_ids])}) "
    end

    tags_table = ""
    tags_where = ""
    if params[:tag_ids].present?
      tags_table = "core_page_tags cpt,"
      tags_where = "AND cpt.tag_id IN (#{param_for_sql_in(params[:tag_ids])}) AND cpt.page_id = pas.page_id "
    end

    if params[:activity_start_dt].present? || params[:activity_end_dt].present?
      if params[:activity_start_dt].present?
        sdt = Time.parse(params[:activity_start_dt]) rescue 2.weeks.ago
      else
        sdt = 2.weeks.ago
      end
      if params[:activity_end_dt].present?
        edt = Time.parse(params[:activity_end_dt]) rescue Time.now.utc
      else
        edt = Time.now.utc
      end
    else
      if params[:page_ids].present? || params[:tag_ids].present?
        sdt, edt = dates_from_page_data(client, params, period)
      else
        sdt = 2.weeks.ago
        edt = Time.now.utc
      end
    end

    if params[:period]
      time_unit = params[:period]
    else
      if sdt && edt
        tdiff = edt - sdt
        if tdiff < 6.hours
          time_unit = 'minute'
        elsif tdiff <= 4.days
          time_unit = 'hour'
        elsif tdiff < 3.months
          time_unit = 'day'
        elsif tdiff < 1.year
          time_unit = 'week'
        else
          time_unit = 'month'
        end
      else
        time_unit = 'day'
      end
    end

    activity_dt_where = ""
    activity_dt_where << "AND period >= '#{sdt.to_s(:db)}' " if sdt
    activity_dt_where << "AND period < '#{edt.to_s(:db)}' "  if edt

    if interval.nil?
      period_clause = "CAST(DATE_TRUNC('#{time_unit}', period AT TIME ZONE '#{tz}') AS TIMESTAMP WITHOUT TIME ZONE)"
      period_epoch_clause = "CAST(EXTRACT(EPOCH from DATE_TRUNC('#{time_unit}', period AT TIME ZONE '#{tz}')) as bigint) * 1000::bigint"
    else
      period_clause = "CAST(tstrunc( period AT TIME ZONE '#{tz}', '#{interval} seconds') AS TIMESTAMP WITHOUT TIME ZONE)"
      period_epoch_clause = "CAST(EXTRACT(EPOCH from tstrunc( period AT TIME ZONE '#{tz}', '#{interval} seconds')) as bigint) * 1000::bigint"
    end

    sql = %Q{
              SELECT ps.page_id,
                     ps.period,
                     ps.period_epoch,
                     cp.title,
                     cp.type,
                     ls.list_size,
                     ps.page_id,
                     ps.actions,
                     CASE WHEN cp.type = 'Unsubscribe' THEN  -ps.actions ELSE ps.new_members END AS members,
                     ps.new_subscribers,
                     ps.revenue
                FROM
                     core_page cp,
                     (
                      SELECT ROUND(AVG(mailable)) AS list_size,
                             #{period_clause} AS period
                        FROM list_size_by_period ls,
                             core_list cl
                       WHERE cl.is_default = true
                         AND cl.id = ls.list_id
                         #{activity_dt_where}
                       GROUP BY #{period_clause}
                     ) AS ls
                     LEFT OUTER JOIN
                     (
                       SELECT pas.page_id,
                              #{period_clause} AS period,
                              #{period_epoch_clause} AS period_epoch,
                              SUM(pas.actions) AS actions,
                              SUM(pas.new_members) AS new_members,
                              SUM(pas.new_subscribers) AS new_subscribers,
                              SUM(pas.revenue) AS revenue
                         FROM #{tags_table}
                              page_action_summary_by_period pas
                        WHERE pas.#{page_where}
                              #{activity_dt_where}
                              #{user_source_where}
                              #{action_source_where}
                              #{tags_where}
                        GROUP BY pas.page_id, #{period_clause}, period_epoch
                     ) AS ps on ls.period = ps.period
               WHERE cp.id = ps.page_id
               ORDER BY ps.page_id, ps.period_epoch #{order}
    }

    rows = nil
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    return rows if json == false

    data = {}
    rows.each do |r|
      pid = r['page_id']
      data[pid] = [] if data[pid].nil?
      data[pid] << {
        :page_id        => pid.to_i,
        :period         => r['period'],
        :period_epoch   => r['period_epoch'].to_i,
        :actions        => r['actions'].to_i,
        :members        => r['members'].to_i,
        :new_subscribes => r['new_subscribes'].to_i,
        :revenue        => r['revenue'].to_i,
      }
    end

    Yajl::Encoder.encode(data)
  end

  private


  def dates_from_page_data(client, params, period)
    # sdt = 2.weeks.ago
    # edt = Time.now.utc
    sdt = edt = nil

    summary_table = ""

    page_where = ""
    page_where = "AND cp.id IN (#{param_for_sql_in(params[:page_ids])})" if params[:page_ids].present?

    page_dt_where = ""
    if params[:page_start_dt].present? || params[:page_end_dt].present?
      page_dt_where << "AND cp.created_at >= '#{params[:page_start_dt]}' AND ps.period >= '#{params[:page_start_dt]}' " if params[:page_start_dt].present?
      page_dt_where << "AND cp.created_at < '#{params[:page_end_dt]}' " if params[:page_end_dt].present?
    end

    activity_dt_where = ""
    if params[:activity_start_dt].present? || params[:activity_end_dt].present?
      activity_dt_where << "AND cp.id = ps.page_id "
      activity_dt_where << "AND ps.period >= '#{params[:activity_start_dt]}' " if params[:activity_start_dt].present?
      activity_dt_where << "AND ps.period < '#{params[:activity_end_dt]}' "    if params[:activity_end_dt].present?
    end

    tags_table = ""
    tags_where = ""
    if params[:tag_ids].present?
      tags_table = "core_page_tags cpt,"
      tags_where = "AND cpt.tag_id IN (#{param_for_sql_in(params[:tag_ids])}) AND cpt.page_id = cp.id "
    end

    sql = %Q{ SELECT MIN(ps.period) AS sdt,
                     MAX(ps.period) AS edt
                FROM page_action_summary_by_period ps,
                     #{tags_table}
                     core_page cp
               WHERE ps.page_id = cp.id
                     #{page_where}
                     #{page_dt_where}
                     #{activity_dt_where}
                     #{tags_where}
               LIMIT 1
    }

    rows = nil
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    if rows
      if rows[0] && rows[0]['sdt']
        sdt = Time.parse(rows[0]['sdt']).utc
        sdt = Time.parse(params[:page_start_dt].to_s) if params[:page_start_dt].present? && sdt > Time.parse(params[:page_start_dt].to_s)
      end

      if rows[0] && rows[0]['edt'] && params[:activity_end_dt].present?
        edt = Time.parse(rows[0]['edt']) + (period != nil ? period.days : 3.days)
        edt = Time.now.utc if edt > Time.now.utc
      else
        edt = Time.now.utc
      end
    end

    [sdt, edt]
  end

    end
  end
end
