module Summary
  module Query
    class << self

  def mailing_clicks_by_link(client, mailing_id, limit: 24, json: false)
    sql = %Q{
              SELECT cc.link_number, 
                     ccu.url, 
                     COUNT(DISTINCT cc.user_id) AS clicks,
                     ROUND(COUNT(DISTINCT cc.user_id)::decimal / total.sent * 100, 2) AS pct
                FROM core_click cc
                     LEFT JOIN core_clickurl ccu ON cc.clickurl_id = ccu.id,
                     (
                       SELECT SUM(sent) AS sent FROM email_sent_summary WHERE mailing_id = ?
                     ) AS total
              WHERE cc.mailing_id = ?
              GROUP BY cc.link_number, ccu.url, total.sent
              ORDER BY clicks DESC
              LIMIT ?
    }
    sql = ActiveRecord::Base.escape_sql([sql, mailing_id, mailing_id, limit])

    rows = nil
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    return rows if json == false

    data = {}
    rows.each do |r|
      lid = r['link_number']
      data[lid] = {}
      data[lid]['link_number']    = r['link_number']
      data[lid]['url']    = r['url']
      data[lid]['clicks'] = r['clicks']
      data[lid]['pct']    = r['pct']
    end

    Yajl::Encoder.encode(data)
  end


    end
  end
end
