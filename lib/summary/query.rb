require 'summary/query/list_activity'
require 'summary/query/list_size'
require 'summary/query/list_size_time_series'
require 'summary/query/list_stats'
require 'summary/query/mailing_aggregate'
require 'summary/query/mailing_summary'
require 'summary/query/mailing_clicks_by_link'
require 'summary/query/mailing_by_email_domain'
require 'summary/query/mailing_sent_by_source'
require 'summary/query/mailing_time_series'
require 'summary/query/mailing_comparison'
require 'summary/query/page_sources'
require 'summary/query/page_summary'
require 'summary/query/page_time_series'

module Summary
  module Query
    class << self

  protected

  def param_for_sql_in(p)
    case p
      when String
        a = p.split(/[ ,]/).select{|x| x.present? }
      when Fixnum
        a = [p]
      when Array
        a = p
      else
        raise "unknown class: #{p.class}"
    end
    
    a = a.collect{|e| e.to_i }
    ActiveRecord::Base.escape_sql(["?", a])
  end

    end
  end
end
