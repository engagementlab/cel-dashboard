#
# Creates a symlink to public/pv.jc from the latest version in public/assets
#
require 'rubygems'
require 'bundler'

APP_PATH = File.expand_path('../../config/application',  __FILE__)
require File.expand_path('../../config/boot',  __FILE__)
require File.expand_path('../../config/environment',  __FILE__)

require 'pp'
require 'json'

path = "#{Rails.root}/assets_manifest.json"

if ! File.exists?(path)
  puts "File not file: path"
  exit(-1)
end

data = File.open(path) do |file|
  JSON.parse(file.read)
end

afname = data['assets']['pv.js']

puts "executing: ln -nfs #{Rails.root}/public/assets/#{afname} #{Rails.root}/public/pv.js"

Kernel.system("ln -nfs #{Rails.root}/public/assets/#{afname} #{Rails.root}/public/pv.js")

exit(0)
