#!/usr/bin/env ruby
#
# Sync's data from ActionKit to CEL Center DB.
#
require 'rubygems'
require 'bundler'

APP_PATH = File.expand_path('../../config/application',  __FILE__)
require File.expand_path('../../config/boot',  __FILE__)
require File.expand_path('../../config/environment',  __FILE__)

require 'pp'
require 'yaml'

require 'mysql2'
require 'pg'

require 'summary/populate'


@log = Logger.new("#{Rails.root}/log/ak_sync.log")
@log_level = 2
@sync_log = nil
@refresh = (Rails.env == 'production' ? true : false)

BATCH_SIZE = 10000

def max(a, b)
  if a.to_i > b.to_i
    a.to_i
  else
    b.to_i
  end
end

def sync_error(db, table)
  @sync_log.status = 'failed'
  @sync_log.err_msg = "#{db}.#{table}: failed"
  @sync_log.save!
  exit(1)
end

def log(level, msg)
  if level <= @log_level
    @log.info("ak_sync: " + ("  " * level) + msg)
    if Rails.env == 'development' || @log_level > 2
      puts ("  " * level) + msg
    end
  end
end


# Extract the database names from config/database.yml
def ak_databases
  db_yaml = YAML::load(open("#{Rails.root}/config/ak_database.yml"))
  dbs = {}
  db_yaml.each do |d|
    dbs[d[0].to_sym] = d[1] if d[1].class == Hash
  end
  dbs
end

def mysql(db)
  @myconn ||= begin
    dsn = ak_databases()[db]
    conn = Mysql2::Client.new(dsn)
    conn.query('SET SESSION join_buffer_size = 1024 * 1024 * 1024')
    conn.query('SET SESSION sort_buffer_size = 1024 * 1024 * 1024')
    conn.query('SET SESSION tmp_table_size = 1024 * 1024 * 1024')
    conn.query('SET SESSION max_heap_table_size = 1024 * 1024 * 1024')
    conn.query('SET SESSION SQL_BIG_SELECTS = 1')
    conn.query('SET SESSION interactive_timeout = 86400')
    conn.query('SET SESSION net_read_timeout    = 86400')
    conn.query('SET SESSION net_write_timeout   = 86400')
    conn.query('SET SESSION wait_timeout        = 86400 ')
    conn
  end
end

def cel_dsn
  dsn = YAML::load(open("#{Rails.root}/config/database.yml"))[Rails.env]
  dsn['dbname'] = dsn['database']
  dsn['user']   = dsn['username']

  dsn.delete('database')
  dsn.delete('username')
  dsn.delete('adapter')
  dsn.delete('encoding')
  dsn.delete('pool')
  dsn.delete('template')

  dsn
end

def pgsql(schema)
  @pgconn ||= begin
    conn = PG::Connection.new(cel_dsn)
    conn.set_client_encoding('utf8') 
    conn.exec( "SET SCHEMA '#{schema}';")
    conn
  end
end

def get_simple_tables(db)

  database = ak_databases()[db]['database']

  sql = "SELECT TABLE_NAME AS `table`, COLUMN_NAME AS pkey
           FROM information_schema.COLUMNS
          WHERE TABLE_SCHEMA = '#{database}'
            AND COLUMN_KEY = 'PRI'
            AND COLUMN_NAME LIKE '%id'
            AND TABLE_NAME NOT LIKE 'core_mailingset_%'
            AND TABLE_NAME NOT LIKE 'mergefile_%'
            AND TABLE_NAME NOT LIKE 'tastypie_%'
            AND TABLE_NAME NOT LIKE 'celery_%'
            AND TABLE_NAME NOT LIKE 'django_%'
            AND TABLE_NAME NOT LIKE 'TCB_%'
            AND TABLE_NAME NOT IN ('cache', 'zip_proximity', 'axes_accessattempt')
          ORDER BY TABLE_NAME, COLUMN_NAME"
  mysql(db).query(sql)
end

def get_columns_from_src(db, table)
  src_sql = "SELECT * FROM #{table} LIMIT 1"
  result = mysql(db).query(src_sql)
  result.fields.to_set
end

def get_columns_from_dst(db, table)
  sql = "SELECT * FROM \"#{db}\".\"#{table}\" LIMIT 1"
  pgconn = pgsql(db)
  results = pgconn.exec(sql)
  results.fields.to_set
end

def common_columns(db, table)
  src_fields = get_columns_from_src(db, table)
  dst_fields = get_columns_from_dst(db, table)

  src_fields.intersection(dst_fields).to_a.sort
end

def get_max_field_info_for_dest(db, table, pkey = nil)
  pgconn = pgsql(db)
  sql = "SELECT * FROM \"#{db}\".\"#{table}\" LIMIT 1"
  results = pgconn.exec(sql)

  possible_fields = ['created_at', 'updated_at']
  possible_fields << pkey if pkey

  actual_fields = []
  possible_fields.each do |field|
    if results.fields.include?(field)
      actual_fields << "MAX(#{field}) AS max_#{field}"
    end
  end

  sql = "SELECT "
  sql << actual_fields.join(', ')
  sql << " FROM \"#{db}\".\"#{table}\" LIMIT 1"

  results = pgconn.exec(sql)

  results.first
end


def sync_simple_table_new_records(db, table, pkey, start_id = nil)
  log(3, "sync_simple_table_new_records(): start_id= #{start_id}")

  floor     = start_id
  row_count = 0
  sdur      = 0
  ddur      = 0

  fields = common_columns(db, table)

  while true

    src_sql = %Q{ SELECT `#{fields.join('`, `')}` FROM #{table} WHERE #{pkey} > #{floor} ORDER BY #{pkey} ASC LIMIT #{BATCH_SIZE} }

    log(6, "B: #{src_sql}")

    st1 = Time.now
    rows = mysql(db).query(src_sql)
    sdur += Time.now - st1

    log(4, "src rows: #{rows.size}")

    break if rows.size == 0

    dst_sql = "INSERT INTO \"#{db}\".\"#{table}\" (\"#{fields.join('", "')}\") values "
    place_holders = []

    fields.size.times do |i|
      place_holders << "$#{i+1}"
    end
    dst_sql << "(#{place_holders.join(', ')})"

    log(6, "C: #{dst_sql}")

    dt1 = Time.now
    pgconn = pgsql(db)
    pgconn.exec("BEGIN")

    last_pkey_val  = 0

    rows.each do |row|
      begin
        log(7, "#{row.values}")

        # Nonesense required because AK doesn't not validate the
        # encoding of the data before they store it in their DB.
        # Also, MySQL (< 5.5) is lame with encoding.
        if ['core_actionfield', 'core_userfield', 'events_eventfield', 'events_eventsignupfield'].include?(table)
          if row['value'].valid_encoding? == false
            row['value'].encode!('UTF-8', 'ASCII',  invalid: :replace, undef: :replace, replace: '')
          end
        end

        row.values.map{ |x| x.gsub!(/\0/,'') if x.class == String } # Need to scrub nulls from strings. Sigh
        pgconn.exec_params(dst_sql, row.values)
        last_pkey_val = row[pkey]
        row_count += 1

      rescue => e
        pgconn.exec("ROLLBACK")
        puts(e.message)
        puts(e.backtrace.join("\n"))
        Rails.logger.error(e.message)
        Rails.logger.error(e.backtrace.join("\n"))
        sync_error(db, table)
      end
    end
    pgconn.exec("COMMIT")
    ddur += Time.now - dt1

    break if rows.size < BATCH_SIZE

    floor = last_pkey_val
  end

  log(4, "total rows: #{row_count}")
  [row_count, sdur, ddur]
end


def sync_simple_table_update_records(db, table, pkey, max_id, ts_column, max_ts)
  log(3, "sync_simple_table_update_records(): pkey= #{pkey} max_id= #{max_id} ts_column= #{ts_column} max_ts= #{max_ts}")

  row_count = 0
  floor     = 0
  sdur      = 0
  ddur      = 0

  dest_fields = common_columns(db, table)

  fields_to_select = dest_fields
  fields_to_set = dest_fields.reject{|x| x == pkey}

  place_holders = []
  i = 0
  dest_fields.each do |f|
    if f != pkey
      place_holders << "$#{i + 1}"
      i += 1
    end
  end

  dst_sql = "UPDATE \"#{db}\".\"#{table}\" SET (\"#{fields_to_set.join('", "')}\") = (#{place_holders.join(', ')}) WHERE #{pkey} = $#{i+1}"

  while true

    break if floor >= max_id

    src_sql = %Q{ SELECT `#{fields_to_select.join('`, `')}`
                    FROM #{table}
                   WHERE #{pkey} > #{floor}
                     AND #{pkey} <= #{max_id}
                     AND #{ts_column} > '#{max_ts}'
                   ORDER BY #{ts_column} ASC
                   LIMIT #{BATCH_SIZE}
                }

    log(6, "C: #{src_sql}")

    st1 = Time.now
    rows = mysql(db).query(src_sql)
    sdur += Time.now - st1

    log(4, "src rows: #{rows.size}")
    break if rows.size == 0

    log(6, "B: #{dst_sql}")

    dt1 = Time.now
    pgconn = pgsql(db)
    pgconn.exec("BEGIN")

    last_pkey_val = 0

    rows.each do |row|
      begin
        values = row.to_h.except(pkey).values
        values << row[pkey]
        pgconn.exec_params(dst_sql, values)
        last_pkey_val = row[pkey]
        row_count += 1

      rescue => e
        pgconn.exec("ROLLBACK")
        Rails.logger.error(e.message)
        Rails.logger.error(e.backtrace.join("\n"))
        sync_error(db, table)
      end
    end

    pgconn.exec("COMMIT")
    ddur += Time.now - dt1

    break if rows.size < BATCH_SIZE

    floor = last_pkey_val

  end
  log(4, "total rows: #{row_count}")

  [row_count, sdur, ddur]
end


def sync_simple_table(db, table, pkey, max_info)
  t1 = Time.now.utc

  rows_inserted = rows_updated = 0
  sd1 = sd2 = dd1 = dd2 = 0

  # get max id and max updated_at from dest
  #  - update where updated_at > max and id <= max_id
  #  - insert where id > max_id

  log(1, "#{db}.#{table}(#{pkey}): \t sync_table()")

  if max_info["max_#{pkey}"]
    max_id = max_info["max_#{pkey}"].to_i

    if max_info['max_updated_at']
      ts_column = 'updated_at'
      max_ts = max_info['max_updated_at']
    elsif max_info['max_created_at']
      ts_column = 'created_at'
      max_ts = max_info['max_created_at']
    else
      ts_column = nil
    end
        
    log(3, "Dest has data: max_id= #{max_id}  ts_column= #{ts_column}  max_ts= #{max_ts}")

    rows_updated,  sd1, dd1 = sync_simple_table_update_records(db, table, pkey, max_id, ts_column, max_ts) if ts_column
    rows_inserted, sd2, dd2 = sync_simple_table_new_records(db, table, pkey, max_id)
  else
    log(3, "Dest is empty")
    rows_inserted, sd1, dd1 = sync_simple_table_new_records(db, table, pkey, 0)
  end

  SyncDetailLog.create!(sync_log_id: @sync_log.id,
                        table_name: table,
                        inserted: rows_inserted,
                        updated: rows_updated,
                        duration: Time.now.utc - t1,
                        src_dur: sd1 + sd2,
                        dst_dur: dd1 + dd2)
end


def truncate_table(db, table)
  pgconn = pgsql(db)
  pgconn.exec("TRUNCATE \"#{db}\".\"#{table}\" RESTART IDENTITY")
end

#
# Tables with a primary key but not timestamp
#
def reload_table(db, table)
  t1 = Time.now.utc

  log(1, "#{db}.#{table}: \t reload_table()")

  src_sql = "SELECT * FROM #{table}"

  log(6, "B: #{src_sql}")

  st1 = Time.now
  rows = mysql(db).query(src_sql)
  sdur = Time.now - st1

  log(4, "src rows: #{rows.size}")

  row_count = ddur = 0

  if rows.size > 0

    dst_sql = "INSERT INTO \"#{db}\".\"#{table}\" (#{rows.fields.map{|x| "\"#{x}\""}.join(', ')}) values "
    place_holders = []

    rows.fields.size.times do |i|
      place_holders << "$#{i+1}"
    end
    dst_sql << "(#{place_holders.join(', ')})"

    log(5, "C: #{dst_sql}")

    dt1 = Time.now
    pgconn = pgsql(db)
    pgconn.exec("BEGIN")

    truncate_table(db, table)

    rows.each do |row|
      begin
        log(7, "#{row.values}")
        row.values.map{ |x| x.gsub!(/\0/,'') if x.class == String } # Need to scrub nulls from strings. Sigh
        pgconn.exec_params(dst_sql, row.values)
        row_count += 1
      rescue => e
        pgconn.exec("ROLLBACK")
        Rails.logger.error(e.message)
        Rails.logger.error(e.backtrace.join("\n"))
        sync_error(db, table)
      end
    end
    pgconn.exec("COMMIT")
    pgconn.exec("VACUUM FULL \"#{db}\".\"#{table}\"")
    ddur = Time.now - dt1
  end
  log(4, "total rows: #{row_count}")

  SyncDetailLog.create!(sync_log_id: @sync_log.id,
                        table_name: table,
                        copied: row_count,
                        duration: Time.now.utc - t1,
                        src_dur: sdur,
                        dst_dur: ddur)
end

#
# core_open & core_click
#
def sync_table_without_pkey(db, table)
  t1 = Time.now.utc

  log(1, "#{db}.#{table}: \t sync_table_without_pkey()")

  max_table_info = get_max_field_info_for_dest(db, table)
  max_ts = max_table_info['max_created_at']

  src_sql = "SELECT * FROM #{table} WHERE created_at > '#{max_ts}'"

  log(6, "B: #{src_sql}")

  st1 = Time.now
  rows = mysql(db).query(src_sql)
  sdur = Time.now - st1

  log(4, "src rows: #{rows.size}")

  row_count = ddur = 0

  if rows.size > 0

    dst_sql = "INSERT INTO \"#{db}\".\"#{table}\" (#{rows.fields.map{|x| "\"#{x}\""}.join(', ')}) values "
    place_holders = []

    rows.fields.size.times do |i|
      place_holders << "$#{i+1}"
    end
    dst_sql << "(#{place_holders.join(', ')})"

    log(6, "C: #{dst_sql}")

    dt1 = Time.now
    pgconn = pgsql(db)
    pgconn.exec("BEGIN")

    rows.each do |row|
      begin
        log(7, "#{row.values}")
        row.values.map{ |x| x.gsub!(/\0/,'') if x.class == String } # Need to scrub nulls from strings. Sigh
        pgconn.exec_params(dst_sql, row.values)
        row_count += 1
      rescue => e
        pgconn.exec("ROLLBACK")
        Rails.logger.error(e.message)
        Rails.logger.error(e.backtrace.join("\n"))
        sync_error(db, table)
      end
    end
    pgconn.exec("COMMIT")
    ddur = Time.now - dt1
  end
  log(4, "total rows: #{row_count}")

  SyncDetailLog.create!(sync_log_id: @sync_log.id,
                        table_name: table,
                        inserted: row_count,
                        duration: Time.now.utc - t1,
                        src_dur: sdur,
                        dst_dur: ddur)
end


def sync_subscription_tables(db)

  log(1, "#{db}.sync_subscription_tables()")

  csh_max_info = get_max_field_info_for_dest(db, 'core_subscriptionhistory', 'id')

  sync_simple_table(db, 'core_subscriptionhistory', 'id', csh_max_info)

  cs_max_info = get_max_field_info_for_dest(db, 'core_subscription', 'id')

  pgconn = pgsql(db)
  pgconn.exec("BEGIN")

  # 1) apply unsubscribe actions since csh_max to core_subsription
  # 2) insert new rows to core_subscription since cs_max

  csh_sql = %Q{ SELECT *
                  FROM "#{db}".core_subscriptionhistory cs,
                       "#{db}".core_subscriptionchangetype cshct
                 WHERE cs.id > #{csh_max_info['max_id'].to_i}
                   AND cs.change_id = cshct.id
                   AND POSITION('unsubscribe' IN cshct.name) = 1
              }
  log(6, csh_sql)

  t1 = Time.now
  csh_del_rows = pgconn.exec(csh_sql)
  sd1 = Time.now - t1

  del_sql = %Q{ DELETE from "#{db}".core_subscription where user_id = $1 and list_id = $2 }

  if csh_del_rows.cmd_tuples() > 0
    log(1, "#{db}.core_subscription: \t deleting rows: #{csh_del_rows.cmd_tuples() }")
    log(6, del_sql)
  end

  t1 = Time.now
  csh_del_rows.each do |csh|
    pgconn.exec(del_sql, [csh['user_id'].to_i, csh['list_id'].to_i])
    log(7, "#{csh['user_id']}  #{csh['list_id']}")
  end
  pgconn.exec("COMMIT")
  dd1 = Time.now - t1

  log(1, "#{db}.core_subscription: \t new records")

  rows_inserted, sd2, dd2 = sync_simple_table_new_records(db, 'core_subscription', 'id', cs_max_info['max_id'].to_i)

  SyncDetailLog.create!(sync_log_id: @sync_log.id,
                        table_name: 'core_subscription',
                        inserted: rows_inserted,
                        deleted: csh_del_rows.cmd_tuples(),
                        duration: Time.now.utc - t1,
                        src_dur: sd1 + sd2,
                        dst_dur: dd1 + dd2)
  log(1, "#{db}.sync_subscription_tables(): done")
end


def already_running(db)
  result = SyncLog.where(:schema_name => db).order(:id => :desc).first()

  if result && result['status'] == 'running'
    log(0, "instance running")
    true
  else
    false
  end
end


def refresh_sumary_tables(db)
  return if @refresh == false

  c = Client.find_by_ak_schema_name(db)
  if c
    log(1, "#{db}: \t Summary::Populate.refresh()")

    t1 = Time.now
    Summary::Populate.refresh(c)
    t2 = Time.now
    SyncDetailLog.create!(sync_log_id: @sync_log.id,
                          table_name: 'Summary::Populate.refresh()',
                          inserted: 0,
                          deleted: 0,
                          duration: t2 - t1,
                          src_dur: 0,
                          dst_dur: t2 - t1)
  end
end


def sync_database(db)

  log(0, "sync_database(#{db})")

  # 1) Tables with an integer primary key
  # 2) Tables with a NON- integer primary key
  # 3) Tables WITHOUT a primary key

  t1 = Time.now.utc

  result = check_schema(db)
  log(0, "sync_database(#{db}) check_schema()= #{result}")
  return if result == false

  log(0, "#{db}  start: #{t1}")

  @sync_log = SyncLog.create!(:schema_name => db, :pid => Process.pid, :status => 'running', :start_ts => Time.now.utc)

  max_table_info = {}

  syncable_tables = SyncTable.where(:schema_name => db).order(:table_name)

  syncable_tables.each do |t|

    case t.sync_method
      when 'insert_only', 'update_and_insert'
        max_table_info[t.table_name] = get_max_field_info_for_dest(db, t.table_name, t.pkey)
        sync_simple_table(db, t.table_name, t.pkey, max_table_info[t.table_name])

      when 'no_pkey'
        sync_table_without_pkey(db, t.table_name)

      when 'reload'
        reload_table(db, t.table_name)

      when 'subscription'
        # postpone till all other tables are sync'ed

      when 'ignore', 'unknown'
        # ignore

      else
        raise "unknown sync method! #{t.table_name}: #{t.sync_method}"
    end
  end

  sync_subscription_tables(db)

  refresh_sumary_tables(db)

  @sync_log.status   = 'ok'
  @sync_log.end_ts   = Time.now.utc
  @sync_log.duration = @sync_log.end_ts - @sync_log.start_ts
  @sync_log.save!

  t2 = Time.now.utc
  log(0, "#{db}  complete: #{t2}  (#{t2 - t1} seconds)")
end


# ---------------------------------------------------------------------


def update_src_column_counts(db)

  sql = %Q{ SELECT TABLE_NAME AS table_name, COUNT(1) AS src_col_cnt
              FROM information_schema.COLUMNS c
             WHERE c.TABLE_SCHEMA = '#{ak_databases()[db]['database']}'
               AND c.TABLE_NAME NOT LIKE 'core_mailingset_%'
               AND c.TABLE_NAME NOT LIKE 'mergefile_%'
               AND c.TABLE_NAME NOT LIKE 'django%'
               AND c.TABLE_NAME NOT LIKE 'celery%'
               AND c.TABLE_NAME NOT LIKE 'tastypie%'
               AND c.TABLE_NAME NOT IN ('cache', 'axes_accessattempt', 'zip_proximity', 'TCB_aol_bad_bounces')
             GROUP BY TABLE_NAME
             ORDER BY TABLE_NAME
  }
  begin
    rows = mysql(db).query(sql)

    SyncTable.transaction do
      rows.each do |r|
        st = SyncTable.where(:schema_name => db, :table_name => r['table_name']).limit(1).first()
        if st
          st.src_col_cnt = r['src_col_cnt'] || 0
          st.save!
        else
          log(1, "WARNING: update_src_column_counts(): sync_tables does not include #{db}.#{r['table_name']}")
        end
      end
    end

  rescue => e
    $stderr.puts(e.message)
    $stderr.puts(e.backtrace.join("\n"))
    exit(1)
  end

end

def update_dst_column_counts(db)
  sql = %Q{
    SELECT c.table_name, COUNT(1) AS dst_col_cnt
      FROM information_schema.columns c
     WHERE c.table_schema = '#{db}'
       AND c.table_name NOT LIKE 'core_mailingset_%'
       AND c.table_name NOT LIKE 'mergefile_%'
       AND c.table_name NOT LIKE 'django%'
       AND c.table_name NOT LIKE 'celery%'
       AND c.table_name NOT LIKE 'tastypie%'
       AND c.table_name NOT IN ('cache', 'axes_accessattempt', 'zip_proximity', 'TCB_aol_bad_bounces')
     GROUP BY table_name
     ORDER BY table_name
  }

  begin
    SyncTable.transaction do
      rows = SyncTable.connection.execute(sql)
      rows.each do |r|
        st = SyncTable.where(:schema_name => db, :table_name => r['table_name']).limit(1).first()
        if st
          st.dst_col_cnt = r['dst_col_cnt'] || 0
          st.save!
        else
          log(1, "WARNING: update_dst_column_counts(): sync_tables does not include #{db}.#{r['table_name']}")
        end
      end
    end

  rescue => e
    $stderr.puts(e.message)
    $stderr.puts(e.backtrace.join("\n"))
    exit(1)
  end
end


# Check Schema
#
# Compares AK tables to local tables for db. Checks whether both
# databases contain the same tables, and the column counts for
# the common tables.
#
# - check if AK has new or deleted tables
#   - insert new tables into sync_tables
#   - flag deleted tables
# - for all tables to be synced compare the 
#   number of src table columns to the dst table
#
def check_schema(db, report = false)

  log(0, "check_schema(#{db}, #{report})")

  #
  # First get the require info to check the tables in the src and dest databses.
  #

  src_tables = {}
  dst_tables = {}
  tracked_tables = {}

  # Get src table info
  sql = %Q{ SELECT TABLE_NAME AS table_name, COUNT(1) AS src_col_cnt
              FROM information_schema.COLUMNS c
             WHERE c.TABLE_SCHEMA = '#{ak_databases()[db]['database']}'
               AND c.TABLE_NAME NOT LIKE 'core_mailingset_%'
               AND c.TABLE_NAME NOT LIKE 'mergefile_%'
               AND c.TABLE_NAME NOT LIKE 'django%'
               AND c.TABLE_NAME NOT LIKE 'celery%'
               AND c.TABLE_NAME NOT LIKE 'tastypie%'
               AND c.TABLE_NAME NOT IN ('cache', 'axes_accessattempt', 'zip_proximity', 'TCB_aol_bad_bounces')
             GROUP BY TABLE_NAME
             ORDER BY TABLE_NAME
  }
  rows = mysql(db).query(sql)
  rows.each do |r|
    src_tables[r['table_name']] = r
  end

  # Get dest table info
  sql = %Q{ SELECT c.table_name, COUNT(1) AS dst_col_cnt
              FROM information_schema.columns c
             WHERE c.table_schema = '#{db}'
               AND c.table_name NOT LIKE 'core_mailingset_%'
               AND c.table_name NOT LIKE 'mergefile_%'
               AND c.table_name NOT LIKE 'django%'
               AND c.table_name NOT LIKE 'celery%'
               AND c.table_name NOT LIKE 'tastypie%'
               AND c.table_name NOT IN ('cache', 'axes_accessattempt', 'zip_proximity', 'TCB_aol_bad_bounces')
             GROUP BY table_name
             ORDER BY table_name
  }

  pgconn = pgsql(db)
  rows = pgconn.exec(sql)
  rows.each do |r|
    dst_tables[r['table_name']] = r
  end

  sql = %Q{ SELECT id, table_name, src_col_cnt, dst_col_cnt, sync_method
              FROM public.sync_tables
             WHERE schema_name = '#{db}'
             ORDER BY table_name
  }
  pgconn = pgsql(db)
  rows = pgconn.exec(sql)
  rows.each do |r|
    tracked_tables[r['table_name']] = r
  end

  #
  # Do the comparison
  #

  report = ''
  new_problem = false

  src_tables_ok = true
  src_tables.each do |tname, tbl|
    if tracked_tables[tname]
      tracked_tables[tname]['src_table_exists'] = true
      if tbl['src_col_cnt'].to_i == tracked_tables[tname]['src_col_cnt'].to_i
        tracked_tables[tname]['status'] = 'ok'
      else
        src_tables_ok = false
        tracked_tables[tname]['status'] = 'src_col_cnt mismatch'
        tbl['status'] = "src_col_cnt mismatch: expected: #{tracked_tables[tname]['src_col_cnt'].to_i}  got: #{tbl['src_col_cnt'].to_i}"
        report << "    #{tname}: Wrong src col cnt: expected: #{tracked_tables[tname]['src_col_cnt'].to_i}  got: #{tbl['src_col_cnt'].to_i} \n"
      end
    else
      tbl['status'] = 'new'
      src_tables_ok = false
    end
  end

  dst_tables_ok = true
  dst_tables.each do |tname, tbl|
    if tracked_tables[tname]
      tracked_tables[tname]['dst_table_exists'] = true
      if tbl['dst_col_cnt'].to_i == tracked_tables[tname]['dst_col_cnt'].to_i
        tracked_tables[tname]['status'] = 'ok'
      else
        tracked_tables[tname]['status'] = "dst_col_cnt mismatch: expected= #{tracked_tables[tname]['dst_col_cnt']}  got: dst_col_cnt= #{tbl['dst_col_cnt']}"
        tbl['status'] = "dst_col_cnt mismatch: expected= #{tracked_tables[tname]['dst_col_cnt']}  got: dst_col_cnt= #{tbl['dst_col_cnt']}"
        dst_tables_ok = false
        report << "    #{tname}: Wrong dst col cnt: expected= #{tracked_tables[tname]['dst_col_cnt']}  got: dst_col_cnt= #{tbl['dst_col_cnt']} \n"
      end
    else
      tbl['status'] = 'new'
      dst_tables_ok = false
    end
  end

  #
  # Record/report the comparison results
  #

  ok_to_sync = true

  begin

    src_tables.each do |tname, t|
      if t['status'] && t['status'] == 'new'
        report << "    new src table: #{t['table_name']} \n"
        SyncTable.create!(:schema_name => db, :table_name => tname, :sync_method => 'ignore', :err_msg => 'unknown table: src', :from_template => false)
        t['recorded'] = true
      end
    end

    dst_tables.each do |tname, t| 
      if t['status'] && t['status'] == 'new' && ! src_tables[tname]['recorded']
        report << "    new dst table: #{t['table_name']} \n"
        SyncTable.create!(:schema_name => db, :table_name => tname, :sync_method => 'ignore', :err_msg => 'unknown table: dst', :from_template => false)
      end
    end

    tracked_tables.each do |tname, t|
      if t['src_table_exists'].nil? || t['dst_table_exists'].nil? || (t['status'] != 'ok' && t['sync_method'] != 'ignore')
        st = SyncTable.find(t['id'])
        st.err_msg = ''
        
        if t['src_table_exists'].nil?
          report << "    #{tname}: missing at src \n"
          st.err_msg = "missing from src "
          st.sync_method = 'ignore'
        end

        if t['dst_table_exists'].nil?
          report << "    #{tname}: missing at dst \n"
          st.err_msg += "missing from dst "
          st.sync_method = 'ignore'
        end

        if (t['status'] != 'ok' && t['sync_method'] != 'ignore')
          report << "    #{t['table_name']}: table changed: #{t['status']}"
          log(0, "    #{t['table_name']}: table changed: #{t['status']}")
          st.err_msg += t['status']
          ok_to_sync = false
        end

        new_problem = true if st.changed?

        st.save!
      end
    end

  rescue => e
    report += "\n\n"
    report += e.message
    report += e.backtrace.join("\n")
    log(0, report)
    SyncNotifier.email_report(db, report).deliver
    exit(1)
  end

  if new_problem && report.present?
    report = "\ncheck_schema(#{db}) \n" + report
    log(0, report)
    SyncNotifier.email_report(db, report).deliver
  end

  log(0, "check_schema(#{db}): ok_to_sync= #{ok_to_sync}")

  ok_to_sync
end


# Prepare Database()
# - determine if sync_tables is populated for db
#   - If yes, print warning msg
#   - If no, copy rows from sync_ak_table_templates into
#     sync_tables for db
# - call check_schema(db, true)

def prepare_for_syncing(db)
  if SyncTable.where(:schema_name => db).count() > 0
    $stderr.puts "\nWARNING: database #{db} is already prepared\n\n"
    exit(1)
  end

  sql = %Q{ INSERT INTO public.sync_tables
              (schema_name, table_name, pkey, pkey_type, sync_method, from_template, src_col_cnt, dst_col_cnt, created_at, updated_at) 
              SELECT '#{db}', table_name, pkey, pkey_type, sync_method, true, 0, 0, NOW(), NOW() FROM public.sync_ak_table_templates ORDER BY id
          }

  log(2, sql)

  begin
    pgconn = pgsql(db)
    pgconn.exec(sql)
  rescue => e
    $stderr.puts(e.message)
    $stderr.puts(e.backtrace.join("\n"))
    exit(1)
  end
  update_src_column_counts(db)
  update_dst_column_counts(db)
  check_schema(db, true)
end

def update_column_counts(db)
  update_src_column_counts(db)
  update_dst_column_counts(db)
  check_schema(db, true)
end


# ---------------------------------------------------------------------

def usage(msg = nil)
  $stderr.puts "\n#{msg}\n\n" if msg
  $stderr.puts "ak-sync.rb dbname action [-v | --verbose=n] [--refresh | --no-refresh]"
  $stderr.puts ""
  $stderr.puts "    dbname must exist in config/ak_database.yml"
  $stderr.puts "    action = sync | check | prepare | update-col-counts"
  $stderr.puts "    verbosity: -v"
  $stderr.puts "             : --verbose=n where n = 0 to 9"
  $stderr.puts "             : -v is the same as --verbose=3"
  $stderr.puts ""
  exit(1)
end

usage("ERROR: Wrong number of arguments") if ARGV.size < 2 || ARGV.size > 4
dbname  = ARGV.shift.to_sym

if ak_databases[dbname].blank?
  usage("ERROR: unknown dbname (#{dbname})\n\n  Known databases: " + ak_databases.keys.collect{|x| x.to_s}.join(', '))
end

exit(1) if already_running(dbname)


action  = ARGV.shift

ARGV.each do |arg|
  opt, val = arg.split('=')
  case opt
    when '--refresh'
      @refresh = true
    when '--no-refresh'
      @refresh = false
    when '-v'
      @log_level = 3
    when '--verbose'
      @log_level = (val ? val.to_i : 3)
    else
      usage("ERROR: Unknown option (#{opt})")
  end
end

begin

  case action
    when 'sync'
      sync_database(dbname)
    when 'check'
      check_schema(dbname, true)
    when 'prepare'
      prepare_for_syncing(dbname)
    when 'update-col-counts'
      update_column_counts(dbname)
    else
      usage("ERROR: Unknown action (#{action})")  
  end

rescue => e
  msg = "\nak_sync.rb threw an exception:\n\n"
  msg << "================================\n"
  msg << e.message
  msg << "\n"
  msg << e.backtrace.join("\n")
 
  puts msg if Rails.env == 'development' || @log_level > 2
  Rails.logger.error(msg)

  SyncNotifier.email_report(dbname, msg).deliver
end
