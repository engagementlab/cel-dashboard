#
# Create a CDFA client.
#
require 'rubygems'
require 'bundler'

APP_PATH = File.expand_path('../../config/application',  __FILE__)
require File.expand_path('../../config/boot',  __FILE__)
require File.expand_path('../../config/environment',  __FILE__)

require 'pp'
require 'yaml'

def usage(msg = nil)
  $stderr.puts "\n#{msg}\n\n" if msg
  $stderr.puts "create_client.rb schema_name name email"
  $stderr.puts ""
  $stderr.puts "    schema_name must exist in config/ak_database.yml"
  $stderr.puts ""
  exit(1)
end

# Extract the database names from config/database.yml
def ak_databases
  db_yaml = YAML::load(open("#{Rails.root}/config/ak_database.yml"))
  dbs = {}
  db_yaml.each do |k, v|
    dbs[k.to_sym] = v if v.class == Hash
  end
  dbs
end

usage("ERROR: Wrong number of arguments") if ARGV.size != 3

client_name = ARGV[0]
email = ARGV[2]

schema_name = ARGV[1].to_sym

if schema_name[-3..-1] == '_ak'
  schema_name = schema_name[0..-4]
end

ak_schema_name = "#{schema_name}_ak".to_sym
cc_schema_name = "#{schema_name}_cc".to_sym

if ak_databases[ak_schema_name].blank?
  usage("ERROR: unknown AK dbname (#{ak_schema_name})\n\n  Known databases: " + ak_databases().keys.collect{|x| x.to_s}.join(', '))
end

ak_client = Client.find_by_ak_schema_name(ak_schema_name)
cc_client = Client.find_by_cc_schema_name(cc_schema_name)

usage("ERROR: #{schema_name} is already configured in the clients table") if ak_client || cc_client

ActiveRecord::Base.connection.execute("create schema #{ak_schema_name}")
ActiveRecord::Base.connection.execute("create schema #{cc_schema_name}")

puts 'CLIENTS'
client = Client.find_or_create_by_name(:name => client_name,
                                       :admin => true,
                                       :ak_schema_name => ak_schema_name,
                                       :cc_schema_name => cc_schema_name,
                                       :master_user_id => 0)
puts 'ROLES'
['admin', 'user'].each do |role|
  Role.find_or_create_by_name(role)
  puts 'role: ' << role
end
puts 'DEFAULT USERS'
user = User.find_or_create_by_email(:name => 'CDFA Admin', 
                                    :email => email,
                                    :password => 'ChangeMe321.',
                                    :password_confirmation => 'ChangeMe321.',
                                    :client_id => client.id)
puts 'user: ' << user.name << '  (email: ' << user.email << ')'
user.confirm!
user.add_role :admin

client.master_user_id = user.id
client.save!
