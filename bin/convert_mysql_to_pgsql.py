#!/usr/bin/env python3

#
# Originates from https://github.com/lanyrd/mysql-postgresql-converter
#
# Heavily modified for the specific needs of converting AK databases
# for the CEL Center Dastboard.
#


"""
Fixes a MySQL dump made with the right format so it can be directly
imported to a new PostgreSQL database.

Dump using:
mysqldump --compatible=postgresql --default-character-set=utf8 -r databasename.mysql -u root databasename
"""

import re
import sys
import os
import time
import subprocess

def table_blacklisted(table_name):
    blacklist = [
        'axes_accessattempt',
        'cache',
        'celery',
        'core_mailingset_',
        'mergefile_',
        'django_',
        'tastypie_']
    for t in blacklist:
      if table_name.startswith(t) == True:
        return True
    return False


def parse(input_filename, output_filename, schema):
    "Feed it a file, and it'll output a fixed one"

    # State storage
    if input_filename == "-":
        num_lines = -1
    else:
        num_lines = int(subprocess.check_output(["wc", "-l", input_filename]).strip().split()[0])
    tables = {}
    current_table = None
    creation_lines = []
    foreign_key_lines = []
    sequence_lines = []
    cast_lines = []
    num_inserts = 0
    started = time.time()

    # Open output file and write header. Logging file handle will be stdout
    # unless we're writing output to stdout, in which case NO PROGRESS FOR YOU.
    if output_filename == "-":
        output = sys.stdout
        logging = open(os.devnull, "w")
    else:
        output = open(output_filename, "w")
        logging = sys.stdout

    if input_filename == "-":
        input_fh = sys.stdin
    else:
        input_fh = open(input_filename, 'rt', 1, 'utf_8', 'replace')


    output.write("-- Converted by db_converter\n")
    # output.write("START TRANSACTION;\n")
    output.write("SET standard_conforming_strings=off;\n")
    output.write("SET escape_string_warning=off;\n")
    output.write("SET CONSTRAINTS ALL DEFERRED;\n\n")

    # output.write("DROP SCHEMA \"%s\" CASCADE;\n\n" % schema)
    # output.write("CREATE SCHEMA \"%s\";\n\n" % schema)

    for i, line in enumerate(input_fh):
        time_taken = time.time() - started
        percentage_done = (i+1) / float(num_lines)
        secs_left = (time_taken / percentage_done) - time_taken
        if (i % 100) == 0:
            logging.write("\rLine %i (of %s: %.2f%%) [%s tables] [%s inserts] [ETA: %i min %i sec]" % (
                i + 1,
                num_lines,
                ((i+1)/float(num_lines))*100,
                len(tables),
                num_inserts,
                secs_left // 60,
                secs_left % 60,
            ))
            logging.flush()
        # line = line.decode("utf8").strip().replace(r"\\", "WUBWUBREALSLASHWUB").replace(r"\'", "''").replace("WUBWUBREALSLASHWUB", r"\\")
        line = line.strip().replace(r"\\", "WUBWUBREALSLASHWUB").replace(r"\'", "''").replace("WUBWUBREALSLASHWUB", r"\\")
        # Ignore comment lines
        if line.startswith("--") or line.startswith("/*") or line.startswith("LOCK TABLES") or line.startswith("DROP TABLE") or line.startswith("UNLOCK TABLES") or not line:
            continue

        # Outside of anything handling
        if current_table is None:
            # Start of a table creation statement?
            if line.startswith("CREATE TABLE"):
                current_table = line.split('"')[1]

                if table_blacklisted(current_table) == True:
                    ignore_table = True
                else:
                    output.write("DROP TABLE IF EXISTS \"%s\".\"%s\";\n" % (schema, current_table))
                    output.write("CREATE TABLE \"%s\".\"%s\" (\n" % (schema, current_table))
                    ignore_table = False

                tables[current_table] = {"columns": []}
                creation_lines = []

            # Inserting data into a table?
            elif ignore_table == False and line.startswith("INSERT INTO"):
                if ignore_table == False:
                    line = line.replace("INSERT INTO " , "INSERT INTO \"%s\"." % schema)
                    line = line.replace("'0000-00-00 00:00:00'", "NULL") + "\n"
                    output.write(line)
                    num_inserts += 1
            # ???
            else:
                sys.stderr.write("\n ! Unknown line in main body: %s\n" % line)

        # Inside-create-statement handling
        else:
            # Is it a column?
            if line.startswith('"'):
                useless, name, definition = line.strip(",").split('"',2)
                try:
                    type, extra = definition.strip().split(" ", 1)
                except ValueError:
                    type = definition.strip()
                    extra = ""
                extra = re.sub("CHARACTER SET [\w\d]+\s*", "", extra.replace("unsigned", ""))
                # See if it needs type conversion
                final_type = None
                if type == "tinyint(1)":
                    type = "int4"
                    final_type = "boolean"
                elif type == "tinyint(4)":
                    type = "int4"
                    final_type = "boolean"
                elif type.startswith("int("):
                    type = "integer"
                elif type.startswith("bigint("):
                    type = "bigint"
                elif type == "longtext":
                    type = "text"
                elif type == "mediumtext":
                    type = "text"
                elif type == "tinytext":
                    type = "text"
                elif type.startswith("varchar("):
                    size = int(type.split("(")[1].rstrip(")"))
                    type = "varchar(%s)" % (size)
                elif type.startswith("smallint("):
                    type = "int2"
                elif type == "datetime":
                    type = "timestamp without time zone"
                elif type == "double":
                    type = "double precision"
                if final_type:
                    cast_lines.append("ALTER TABLE \"%s\".\"%s\" ALTER COLUMN \"%s\" DROP DEFAULT, ALTER COLUMN \"%s\" TYPE %s USING CAST(\"%s\" as %s)" % (schema, current_table, name, name, final_type, name, final_type))
                # ID fields need sequences
                if name == "id":
                    sequence_lines.append("CREATE SEQUENCE %s.%s_id_seq" % (schema, current_table))
                    sequence_lines.append("SELECT setval('%s.%s_id_seq', max(id)) FROM \"%s\".\"%s\"" % (schema, current_table, schema, current_table))
                    sequence_lines.append("ALTER TABLE \"%s\".\"%s\" ALTER COLUMN \"id\" SET DEFAULT nextval('%s.%s_id_seq')" % (schema, current_table, schema, current_table))
                # Record it
                creation_lines.append('"%s" %s %s' % (name, type, extra))
                tables[current_table]['columns'].append((name, type, extra))
            # Is it a constraint or something?
            elif line.startswith("PRIMARY KEY"):
                creation_lines.append(line.rstrip(","))
            elif line.startswith("CONSTRAINT"):
                foreign_key_lines.append("ALTER TABLE \"%s\" ADD CONSTRAINT %s DEFERRABLE INITIALLY DEFERRED" % (current_table, line.split("CONSTRAINT")[1].strip().rstrip(",")))
                # foreign_key_lines.append("CREATE INDEX ON \"%s\".\"%s\" %s" % (schema, current_table, line.split("FOREIGN KEY")[1].split("REFERENCES")[0].strip().rstrip(",")))
            elif line.startswith("UNIQUE KEY"):
                creation_lines.append("UNIQUE (%s)" % line.split("(")[1].split(")")[0])
            elif line.startswith("KEY"):
                pass
            # Is it the end of the table?
            elif line == ");":
                if ignore_table == False:
                    for j, line in enumerate(creation_lines):
                        output.write("    %s%s\n" % (line, "," if j != (len(creation_lines) - 1) else ""))
                    output.write(');\n\n')
                current_table = None
            # ???
            else:
                sys.stderr.write("\n ! Unknown line inside table creation: %s" % line)


    # Finish file
    output.write("\n-- Post-data save --\n")
    # output.write("COMMIT;\n")
    # output.write("START TRANSACTION;\n")

    # Write typecasts out
    output.write("\n-- Typecasts --\n")
    for line in cast_lines:
        output.write("%s;\n" % line)

    # # Write FK constraints out
    # output.write("\n-- Foreign keys --\n")
    # for line in foreign_key_lines:
    #     output.write("%s;\n" % line)

    # # Write sequences out
    # output.write("\n-- Sequences --\n")
    # for line in sequence_lines:
    #     output.write("%s;\n" % line)

    # Finish file
    output.write("\n")
    # output.write("COMMIT;\n")
    output.write("\n\n")
    logging.write("\n")


if __name__ == "__main__":
    parse(sys.argv[1], sys.argv[2], sys.argv[3])

