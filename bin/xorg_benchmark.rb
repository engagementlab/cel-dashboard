#
# Script to run benchmark queries against multiple schema
#
require 'pg'
require 'pp'
require 'yaml'

APP_PATH = File.expand_path('../../config/application',  __FILE__)
require File.expand_path('../../config/boot',  __FILE__)
require File.expand_path('../../config/environment',  __FILE__)

def cel_dsn
  json = YAML::load(open("#{Rails.root}/config/database.yml"))[Rails.env]

  dsn = {}
  dsn['dbname']   = json['database']
  dsn['user']     = json['username']
  dsn['password'] = json['password']

  dsn
end

def pgsql
  @pgconn ||= begin
    conn = PG::Connection.new(cel_dsn)
    conn.set_client_encoding('utf8') 
    conn
  end
end

def get_clients
  clients = Client.where('active = true AND LENGTH(ak_schema_name) > 0 AND ak_schema_name IS NOT NULL')
end

def save_results(ts, report, results)
  path = "#{Rails.root}/xorg_results"
  Dir.mkdir(path) if ! Dir.exist?(path)

  path << "/#{ts}"
  Dir.mkdir(path) if ! Dir.exist?(path)

  filename = "#{path}/#{report}.csv"
  puts "Writing: #{filename}"

  File.open(filename, "w") do |f|
    f.write(results)
  end
end

ts = Time.now.strftime("%Y%m%d%H%M%S")

clients = get_clients()

query_files = []

if ARGV.present?
  ARGV.each do |a|
    if File.exists?(a)
      query_files << a
    else
      puts "File doesn't exist: Skipping: #{a}"
    end
  end
else
  query_files = Dir.glob("#{Rails.root}/lib/xorg_queries/*.pgsql")
end

pgconn = pgsql()
pgconn.type_map_for_results = PG::BasicTypeMapForResults.new(pgconn)


query_files.sort.each do |full_path| 

#  puts "#{full_path}"
  f = open(full_path)
  sql = f.read
  f.close

  # Extract the filename from the path stripping the '.sql' extension
  report = File.basename(full_path).gsub(/\.pgsql/, '')
  print "#{report}\n"

  results = ''

  clients.each_with_index do |client, num|
    begin
      puts "  #{num} #{client.name}"

      pgconn.exec("SET SEARCH_PATH = #{client.ak_schema_name}, public;")
      result = pgconn.exec(sql)
      pgconn.exec("SET SEARCH_PATH = public;")

      #results = "#{(['Organization'] + rows.fields).join(',')}\n" 

      if num == 0
        header = ['"Organization"']
        result.fields.each do |f|
          header << "\"#{f}\""
        end
        results = header.join(',') + "\n"
      end

      result.each_row do |row|
        results << "\"#{client.name}\","
        column = 0
        row.each do |field|
#          puts "#{row.size}  #{column}  #{field.class} #{field} #{field[1].class} #{result.ftype(column)}"
          case field
            when Float, Fixnum, BigDecimal
              results << field.to_s
            else
              if result.ftype(column) == 1700
                results << field.to_s
              else
                results << "\"#{field}\""
              end
          end

          if column < (row.size - 1)
            results << ","
          else
            results << "\n"
          end
          column += 1
        end
      end

    rescue => e
      puts e.message.to_s + "\n" + e.backtrace.join("\n")
      puts "\n\nContinuing...\n"

      next

    end
  end

  save_results(ts, report, results)

  puts

end
