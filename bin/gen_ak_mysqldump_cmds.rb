#!/usr/bin/env ruby
#
#
require 'rubygems'
require 'bundler'

APP_PATH = File.expand_path('../../config/application',  __FILE__)
require File.expand_path('../../config/boot',  __FILE__)
require File.expand_path('../../config/environment',  __FILE__)

require 'pp'
require 'yaml'

require 'mysql2'


#
# The following ActionKit tables store data submitted
# by members. In other words, untrusted data from the
# Internet. In some AK instances these tables have a few
# rows with bad data such that importing the row into 
# Postgres fails. 
#
TABLES_WITH_POTENTIALLY_BAD_DATA = [
  'core_actionfield',
  'core_userfield',
  'events_eventfield',
  'events_eventsignupfield'
]


def ak_databases
  db_yaml = YAML::load(open("#{Rails.root}/config/ak_database.yml"))
  dbs = {}
  db_yaml.each do |d|
    dbs[d[0].to_sym] = d[1] if d[1].class == Hash
  end
  dbs
end

def ak_database(db)
  ak_databases()[db]
end

def mysql(db)
  @myconn ||= begin
    dsn = ak_database(db)
    conn = Mysql2::Client.new(dsn)
    conn.query('SET SESSION join_buffer_size = 1024 * 1024 * 1024')
    conn.query('SET SESSION sort_buffer_size = 1024 * 1024 * 1024')
    conn.query('SET SESSION tmp_table_size = 1024 * 1024 * 1024')
    conn.query('SET SESSION max_heap_table_size = 1024 * 1024 * 1024')
    conn.query('SET SESSION SQL_BIG_SELECTS = 1')
    conn.query('SET SESSION interactive_timeout = 86400')
    conn.query('SET SESSION net_read_timeout    = 86400')
    conn.query('SET SESSION net_write_timeout   = 86400')
    conn.query('SET SESSION wait_timeout        = 86400 ')
    conn
  end
end


def usage(msg = nil)
  $stderr.puts "\n#{msg}\n" if msg
  $stderr.puts ""
  $stderr.puts "gen_ak_mysqldump_cmds.rb [-h | --help] dbname"
  $stderr.puts ""
  $stderr.puts "    dbname must exist in config/ak_database.yml"
  $stderr.puts ""
  $stderr.puts "    -h, --help            This message."
  $stderr.puts ""
  exit(1)
end


def write_mysqldump_cnd(db, paranoid, tables)

  dsn = ak_database(db)

  cmd =  "mysqldump --host=#{dsn['host']} --user=#{dsn['username']} \\\n"
  cmd << "          --password $3 \\\n"
  cmd << "          --compatible=postgresql --no-tablespaces \\\n"
  cmd << "          --skip-comments --single-transaction \\\n"
  cmd << "          --quick --default-character-set=utf8 \\\n"
  if paranoid
    cmd << "          --skip-extended-insert \\\n"
    cmd << "          --no-data \\\n"
  end
  cmd << "          #{dsn['database']} \\\n"
  cmd << "          " + tables.join(" \\\n          ")
end


def gen_ak_mysqldump_cmds(db)
  schema = ak_database(db)['database']
  sql = %Q{ SELECT t.TABLE_NAME AS table_name
              FROM information_schema.TABLES t
             WHERE t.TABLE_SCHEMA LIKE '#{schema}'
               AND t.TABLE_NAME NOT LIKE 'core_mailingset_%'
               AND t.TABLE_NAME NOT LIKE 'mergefile_%'
               AND t.TABLE_NAME NOT LIKE 'django%'
               AND t.TABLE_NAME NOT LIKE 'celery%'
               AND t.TABLE_NAME NOT LIKE 'tastypie%'
               AND t.TABLE_NAME NOT IN ('cache', 'axes_accessattempt', 'zip_proximity')
               AND t.TABLE_NAME <> 'R_sent_mails_restore'
               AND t.TABLE_NAME <> 'renamed_core_transactions_core_payment'
               AND t.TABLE_NAME <> 'TCB_aol_bad_bounces'
               AND t.TABLE_NAME NOT LIKE 'ST_%'
               AND t.TABLE_NAME NOT LIKE 'AKIMPORT_%'
             ORDER BY t.TABLE_NAME
          }
  rows = mysql(db).query(sql)
  
  tables = []
  rows.each do |row|
    tables << row['table_name'] unless TABLES_WITH_POTENTIALLY_BAD_DATA.include?(row['table_name'])
  end

  mysqldump_name = "#{db}_mysqldump.mysql"
  script_name    = "#{db}_export.sh"
  pgsql_name     = "#{db}_import.pgsql"

  cmd  = "echo\n"
  cmd += "echo Please enter the database password for your ActionKit instance.\n"
  cmd += "echo\n"

  cmd += "echo Note that you will be prompted to enter the password twice. Once for the\n"
  cmd += "echo tables requiring special handling and again for the remaining tables.\n"
  cmd += "echo There will be a short delay between each password prompt.\n"
  cmd += "echo\n"
  cmd += write_mysqldump_cnd(db, true, TABLES_WITH_POTENTIALLY_BAD_DATA)
  cmd += " > #{mysqldump_name} \n"
  cmd += write_mysqldump_cnd(db, false, tables)
  cmd +=" >> #{mysqldump_name} \n"

  cmd += "/usr/bin/env python3 #{Rails.root}/bin/convert_mysql_to_pgsql.py #{mysqldump_name} #{pgsql_name} #{db} \n"

  cmd += "echo\n"
  cmd += "echo\n"
  cmd += "echo Now import the data into your local Postgresql database using a cmd similar to:\n"
  cmd += "echo\n"
  cmd += "echo \"    psql -U <username> -W <dbname> < #{pgsql_name} \"\n"
  cmd += "echo\n"

  # puts "filename: #{script_name}"
  # puts cmd

  f = File.open(script_name, 'w')
  f.puts(cmd)
  f.close
  File.chmod(0755, script_name)
end

# ---------------------------------------------------------------------

dbname   = nil

usage("ERROR: Wrong number of arguments") if ARGV.size < 1 || ARGV.size > 2
case ARGV[0]
  when '-h', '--help'
    usage()
end

dbname = ARGV[0].to_sym if dbname.nil?

if ak_databases[dbname].blank?
  usage("ERROR: unknown dbname: #{dbname}\n\n  Known databases: " + ak_databases.keys.collect{|x| x.to_s}.join(', '))
end

gen_ak_mysqldump_cmds(dbname)



